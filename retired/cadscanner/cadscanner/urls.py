from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'cadscanner.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^webreport/', include('webreport.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
