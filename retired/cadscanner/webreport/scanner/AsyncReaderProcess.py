import os,subprocess

ExeRoot="/home/enspirea/cpp/StepReader/Linux/bin/"
DataRoot="/home/enspirea/CadScannerWeb/cadscanner/webreport/upload/"

class AsyncReaderProcess(object):
    def __init__ (self, datafile, exename, dataext):
        self.cadfilebody = datafile
        self.exename = exename
        self.cadfileext  = dataext
        self.imgGenParam  = "1 1 1 1 1 1 1 1"
        self.red   = 128
        self.green = 255
        self.blue  = 128
        self.param2 = "0"
        self.param3 = "1"
        self.noAnalysis = 1

    def exeCommand(self, fullCommand, args):
        #print fullCommand
        cmd = [fullCommand] + args.split(' ')
        print cmd
        subprocess.Popen(cmd) #, stderr=subprocess.STDOUT)
        #print out

    def runProcess(self):
        exeFullpath = ExeRoot + self.exename
        dataFullpath = DataRoot + self.cadfilebody + self.cadfileext
        fullcommand = exeFullpath + ' ' 
        paramList = []
        #paramList.append(exeFullpath)
        paramList.append(dataFullpath)
        paramList.append(DataRoot)
        paramList.append(self.imgGenParam)
        paramList.append(ExeRoot)
        paramList.append(self.red)
        paramList.append(self.green)
        paramList.append(self.blue)
        paramList.append(self.param2)
        paramList.append(self.param3)
        paramList.append(dataFullpath)
        paramList.append(self.cadfileext)
        paramList.append(self.noAnalysis)

        args = self.generateCommand(paramList)

        self.exeCommand(exeFullpath, args)
        
    def generateCommand(self, paramList):
        return ' '.join(str(e) for e in paramList)

