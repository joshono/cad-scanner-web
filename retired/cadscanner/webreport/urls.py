from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

from webreport import views

urlpatterns = patterns('',
    url(r'^scan$', views.scan, name='scan'),
    url(r'^report$', views.report, name='report'),
   
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
