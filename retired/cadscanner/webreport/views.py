from django.shortcuts import render
from django.http import HttpResponse
from scanner import AsyncScheduler
import os

# Create your views here.

def scan(request):
    AsyncScheduler.main()
    return HttpResponse("scan was scheduled")

def report(request):
    out = "<table><tr>"
    count = 0
    for img in os.listdir('/home/enspirea/CadScannerWeb/cadscanner/webreport/upload/Pictures'):
        print 'processing img', img
        path = 'media/Pictures'
        test_image = path + '/' + img
        
        out = out + '<td><img src="' + test_image + '"></td>'
        count = count + 1
        if count == 4:
            out = out + "</tr>"
            count = 0

    if count != 0:
        out = out + "</tr>"
    out = out + "</table>"

    #print out
    return HttpResponse(out)
