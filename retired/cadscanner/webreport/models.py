from django.db import models

# Create your models here.
class CadDatafile(models.Model):
    CreatedAt   = models.DateTimeField('date installed')
    Filename    = models.CharField(max_length=1024)
    CADID       = models.CharField(max_length=1024)
    CADVersion  = models.CharField(max_length=16)
    CADFormat   = models.CharField(max_length=16)
    Volume      = models.IntegerField(default=-1)
    Xmax        = models.FloatField(default=-1.0)
    Ymax        = models.FloatField(default=-1.0)
    Zmax        = models.FloatField(default=-1.0)
    DocVersion  = models.IntegerField(default=-1)
