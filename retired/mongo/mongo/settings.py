"""
Django settings for mongo project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from mongoengine import *

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# mongo
DBNAME = 'cadapp'
connect(DBNAME)
AUTHENTICATION_BACKENDS = (
    'mongoengine.django.auth.MongoEngineBackend',
)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#_)_i*f11rbqtkg5xpg0@rva@*ke0vazj%2ceq$(5pa$&ie7#v'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'cadapp',
)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'mongo.urls'

WSGI_APPLICATION = 'mongo.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.dummy',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

MEDIA_ROOT = '/home/enspirea/CADScannerWeb/mongo/cadapp/upload/'
MEDIA_URL = '/media/'
MEDIAFILES_DIRS = (
    os.path.join(BASE_DIR, "media"), MEDIA_ROOT,
)

SUPPORTED_EXT = ['.stp']

READEREXE_ROOT = "/home/enspirea/CADScannerWeb/cpp/StepReader/Linux/bin/"
READERTMP_ROOT = READEREXE_ROOT + "StepReader/"

STLANALYZEREXE_ROOT  = "/home/enspirea/CADScannerWeb/cpp/StlAnalyzer/"
STLANALIZERTMP_ROOT =  STLANALYZEREXE_ROOT + "StlAnalyzer/"
