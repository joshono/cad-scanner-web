from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import Q
from scanner import AsyncReaderScheduler
from analyzer import AsyncAnalyzerScheduler
import os
from models import CadDatafile
import datetime
from mongo.settings import SUPPORTED_EXT
from mongo.settings import MEDIA_ROOT
from mongo.settings import READERTMP_ROOT 

def getFilesScannedBefore(mongoInstances):
    filesScannedBefore = []
    for i in mongoInstances:
        filesScannedBefore.append(i.filename)

    return filesScannedBefore

# Create your views here.
def upload(request):
    # for now it reads a fixed location - upload folder
    # filename is the identifier that contains both identity and version
    filesNew = os.listdir(MEDIA_ROOT)
    filesScannedBefore = CadDatafile.objects.all()
    filesScheduled = []

    for newFileToSchedule in filesNew:
        fileTrunk, fileExtension = os.path.splitext(newFileToSchedule)
        if fileExtension not in SUPPORTED_EXT:
            continue

        if newFileToSchedule in filesScannedBefore:
            continue

        # insert database
        obj = CadDatafile(filename=newFileToSchedule, cadId=newFileToSchedule, cadVersion='1.0', cadFormat=fileExtension, createdAt = datetime.datetime.utcnow())
        obj.save()

        filesScheduled.append(newFileToSchedule)

    msgheader = 'Following files were uploaded:'
    outmsg = ''
    for fn in filesScheduled:
        if len(outmsg):
            outmsg = outmsg + ','
        outmsg = outmsg + fn
 
    return HttpResponse(msgheader + outmsg)
    
def scan(request):
    # list files scheduled for scan
    header = "Following files were scheduled for scan:"

    filesToScan = []
    try:
        fileInstsToScan = CadDatafile.objects.filter(volume=-1)

        for fileInst in fileInstsToScan:
            filesToScan.append(fileInst.filename)
    except:
        # no new objects find
        outmsg = "(all files were scanned before)"
        return HttpResponse(header + outmsg)
   
    # pass files to scheduler
    AsyncReaderScheduler.schedule(filesToScan)

    outmsg = ''
    for fn in filesToScan:
        if len(outmsg):
            outmsg = outmsg + ','
        outmsg = outmsg + fn

    return HttpResponse(header+outmsg)

def analyze(request):
    # read stl and import all JSON files onto DB
    readerOutputs = os.listdir(READERTMP_ROOT)
    filesToAnalyze = []
    for outFile in readerOutputs:
        fileTrunk, fileExtension = os.path.splitext(outFile)
        if fileExtension != '.stl':
            continue

        # filetrunk should be like <fbody>(<ext>)
        import re
        dataFnBody, dataFnExt, dummy = re.split('[()]', fileTrunk)
        orgDataFn = dataFnBody + dataFnExt
 
        # query DB if data was processed before
        objFound = CadDatafile.objects.filter(filename=orgDataFn)
        if len(objFound) > 0:
            # data was processed before
            continue

        filesToAnalyze.append(outFile)
   
    AsyncReaderScheduler.schedule(filesToAnalyze)

    header = "Scheduled file :"
    outmsg = ""
    for fn in filesToAnalyze:
        if len(outmsg):
            outmsg = outmsg + ',':
        outmsg = outmsg + fn

    return HttpResponse(header+outmsg)

def jsonToDB(dirToProcess):
    readerOutputs = os.listdir(dirToProcess)
    for outFile in readerOutputs:
        fileTrunk, fileExtension = os.path.splitext(outFile)
        if fileExtension != '.json':
            continue

        import re
        p = re.compile('^[A-Za-z0-9]+Out_(.+).json$')
        stlTrunk, dummy = os.path.splitext(p.group(1))
        if !stlTrunk:
            continue
        
        # process file
        # FIXME, now stp is hardcoded
        cadFullname = stlTrunk + '.stp'
        # find the database entry to update
        objFound = CadDatafile.objects.filter(filename=cadFullname)
        import json
        json_data = open(dirToProcess+outFile)
        if len(objFound)==0:
            print 'creating newEntry for ', cadFullname
            insertDBRecord(cadFullname, json_data[cadFullname])
        else:
            print 'updating existing entry for ',cadFullname
            updateDBRecord(objFound[0], json_data[cadFullname])

def insertDBRecord(cadFullname, jsonData):
    # how to create DB entry from json???
    # TODO
    obj = CadDatafile(filename=newFileToSchedule, cadId=newFileToSchedule, cadVersion='1.0', cadFormat=fileExtension, createdAt = datetime.datetime.utcnow())
        obj.save()
        
def updateDBRecord(dbRecordInst, jsonData):
    # TODO
 

def report(request):
    # check json reports in tmp folder and store them in database
    readerOutputs = os.listdir(READERTMP_ROOT)
    for outFile in readerOutputs:
        fileTrunk, fileExtension = os.path.splitext(outFile)
        if fileExtension != '.json':
            continue

        #get original filename
        # fileTrunk=<fbody>(<ext>) -> <fbody>.<ext>
        import re
        dataFnBody, dataFnExt, dummy = re.split('[()]', fileTrunk)
        orgDataFn = dataFnBody + dataFnExt
        
        # find org data file from DB
        objFound = CadDatafile.objects.filter(filename=orgDataFn)
        if len(objFound) == 0:
            print 'somehow data file ' + orgDataFn + ' not found in Db, ignoring'
            continue
   
        # for now, set minBox:(x:999,y:999,z:999)
        objFound[0].minBox_x = 999
        objFound[0].minBox_y = 999
        objFound[0].minBox_z = 999
        objFound[0].volume = 999999
   
    # get list of processed data
    try:
        #dataProcessed = CadDatafile.objects.filter(~Q(volume=-1))
        dataProcessed = CadDatafile.objects.filter(volume=-1)
    except:
        # no new objects find
        outmsg = "(no files are available for report)"
        return HttpResponse(outmsg)

    out = "<table>"
    # header
    out = out + "<tr><td>name</td><td>image</td><td>volume</td></tr>"
    count = 0
    for cadInst in dataProcessed:
        # beginning of row
        out = out + "<tr>"

        # filename
        out = out + "<td>" + cadInst.filename + "</td>"

        # add image (+isometric)
        fileTrunk, fileExtension = os.path.splitext(cadInst.filename)
        
        imgfile = fileTrunk + "(" + fileExtension + ")_+ISOMETRIC.jpeg"
        path = 'media/Pictures'
        test_image = path + '/' + imgfile
        out = out + '<td><img src="' + test_image + '" width="100"></td>'

        # params (e.g. volume)
        out = out + "<td>" + str(cadInst.volume) + "</td>"
        
        # end of row 
        out = out + "</tr>"

    out = out + "</table>"

    #print out
    return HttpResponse(out)

