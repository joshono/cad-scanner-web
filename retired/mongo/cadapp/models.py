from django.db import models
#from mongoengine import *
#from mongo.settings import DBNAME
from djangotoolbox.fields import ListField

#connect(DBNAME)

# Create your models here.
class CadDatafile(models.Model):
    filename    = models.CharFiled(max_length=1024)

    createdAt   = models.DateTimeField(auto_now_add=True, null=True)

    cadID       = models.CharField(max_length=1024)
    cadVersion  = models.CharField(max_length=16)
    cadFormat   = models.CharField(max_length=16)
    volume      = models.IntField(default=-1)
    docVersion  = models.IntField(default=-1)

    xmax        = models.FloatField(default=-1.0)
    ymax        = models.FloatField(default=-1.0)
    zmax        = models.FloatField(default=-1.0)

    minBox_x    = models.FloatField(default=-1.0)
    minBox_y    = models.FloatField(default=-1.0)
    minBox_z    = models.FloatField(default=-1.0)

    numSurfaces = models.IntField(default=-1)
    volume      = models.FloatField(default=-1.0)
    surface     = models.FloatField(default=-1.0)

    otherBoxes = ListField()
