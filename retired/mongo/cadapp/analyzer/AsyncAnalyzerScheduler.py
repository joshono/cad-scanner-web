from __future__ import print_function
import os,subprocess,sys
import multiprocessing as mp
from Queue import Queue
from threading import Thread
import threading
import StlAnalyzerProcess

def run(procInst):
    procInst.runProcess()
    #subprocess.check_call([seawatV4x64, swt_nam],cwd=cwd)

def worker(queue):
    """Process files from the queue."""
    for args in iter(queue.get, None):
        try:
            run(args)
        except Exception as e: # catch exceptions to avoid exiting the
                               # thread prematurely
            print('%r failed: %s' % (args, e,), file=sys.stderr)

def schedule(filesToScan=None):
    # populate files
    q = Queue()
    for fn in filesToScan:
        fbody, fext = os.path.splitext(fn)
        if fext == '.stl':
            procInst = StlAnalyzerProcess.StlAnalyzerProcess(fbody)
            q.put_nowait(procInst)

    # start threads
    threads = [Thread(target=worker, args=(q,)) for _ in range(mp.cpu_count()-1)]
    for t in threads:
        t.daemon = True # threads die if the program dies
        t.start()
    for _ in threads: q.put_nowait(None) # signal no more files
    for t in threads: t.join() # wait for completion

    
def main():
    schedule(None)

if __name__ == '__main__':
    mp.freeze_support() # optional if the program is not frozen
    main()
