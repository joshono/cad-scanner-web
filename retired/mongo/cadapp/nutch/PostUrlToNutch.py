import urllib
import json as m_json
import time

keyword = {'stl', 'download', 'file'}
pages = 4
urlsPerPage = 8
interval = 60
listOfUrlFiles = '/tmp/urls'

def getURLList(pageNum):
    query = urllib.urlencode ( { 'q' : keyword } )
    response = urllib.urlopen ( 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&' + query + '&rsz=' + str(urlsPerPage) + '&start=' + str(pageNum)).read()
    json = m_json.loads ( response )
    results = json [ 'responseData' ] [ 'results' ]
    urls = []
    for result in results:
        print result['url']
        title = result['title']
        url = result['url'] 
        urls.append(url)

    writeToFile(urls)

def writeToFile(urls):
    f = open(listOfUrlFiles, 'a')
    for url in urls:
        f.write(url+'\n')
    f.close()
    
def doit():
    urls = []
    for i in range(pages):
        currentPage = i*urlsPerPage
        urls = urls +getURLList(currentPage)
        time.sleep(interval)

doit()
