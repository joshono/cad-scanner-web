from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

from cadapp import views

urlpatterns = patterns('',
    url(r'^upload$', views.upload, name='upload'),
    url(r'^scan$', views.scan, name='scan'),
    url(r'^report$', views.report, name='report'),
   
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
