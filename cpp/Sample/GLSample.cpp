#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>

#include <stdio.h>

float rotation_angle=0;
int msaa=1;

void reshape(int width, int height)
{
  glViewport(0, 0, width, height);
}

void mouse(int button, int state, int x, int y)
{
  if (state==GLUT_DOWN) 
  {
    msaa = !msaa;
    glutPostRedisplay();
  }
}


void display()
{
  int err=0;
  glClear(GL_COLOR_BUFFER_BIT);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-1,1,-1,1,-1,1);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glRotatef(rotation_angle, 0,0,1);

  glColor4f(1,0,0,1);

  if (msaa) 
  {
    glEnable(GL_MULTISAMPLE_ARB);
    printf("msaa on\n");
  } 
  else 
  {
    printf("msaa off\n");
    glDisable(GL_MULTISAMPLE_ARB);
  }

  glRectf(-.5,-.5,.5,.5);
  
  glutSwapBuffers();
  err = glGetError();
  if (err)
    fprintf(stderr, "error\n");
}

int
main (int argc, char** argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_MULTISAMPLE);
  //glutInitDisplayMode( GLUT_RGBA );
  glutCreateWindow(argv[0]);

  //glutDisplayFunc(display);
  //glutMouseFunc(mouse);
  //glutReshapeFunc(reshape);
  
  glutReshapeWindow(400,400);

  printf("%s\n",glGetString(GL_RENDERER));

  rotation_angle=30;

  glutMainLoop();
  return 0;
}
