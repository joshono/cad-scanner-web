#include "STLAnalyzer.h"
#include <fstream>
#include <random>

// http://www.geometrictools.com/LibMathematics/Containment/Containment.html
#include "Wm5MathematicsPCH.h"
#include "Wm5ContBox3.h"
#include "Wm5ApprGaussPointsFit3.h"
#include "Wm5Quaternion.h"
#include <json/json.h>

#define _MAX_PATH 1024
//#define DEBUG_TEST_POINT 1000

namespace Wm5
{
//----------------------------------------------------------------------------
template <typename Real>
Box3<Real> ContAlignedBox (int numPoints, const Vector3<Real>* points)
{
    Vector3<Real> pmin, pmax;
    Vector3<Real>::ComputeExtremes(numPoints, points, pmin, pmax);

    Box3<Real> box;
    box.Center = ((Real)0.5)*(pmin + pmax);
    box.Axis[0] = Vector3<Real>::UNIT_X;
    box.Axis[1] = Vector3<Real>::UNIT_Y;
    box.Axis[2] = Vector3<Real>::UNIT_Z;
    Vector3<Real> halfDiagonal = ((Real)0.5)*(pmax - pmin);
    for (int i = 0; i < 3; ++i)
    {
        box.Extent[i] = halfDiagonal[i];
    }

    return box;
}
//----------------------------------------------------------------------------
template <typename Real>
Box3<Real> ContOrientedBox (int numPoints, const Vector3<Real>* points)
{
    Box3<Real> box = GaussPointsFit3<Real>(numPoints, points);

    // Let C be the box center and let U0, U1, and U2 be the box axes.  Each
    // input point is of the form X = C + y0*U0 + y1*U1 + y2*U2.  The
    // following code computes min(y0), max(y0), min(y1), max(y1), min(y2),
    // and max(y2).  The box center is then adjusted to be
    //   C' = C + 0.5*(min(y0)+max(y0))*U0 + 0.5*(min(y1)+max(y1))*U1 +
    //        0.5*(min(y2)+max(y2))*U2

    Vector3<Real> diff = points[0] - box.Center;
    Vector3<Real> pmin(diff.Dot(box.Axis[0]), diff.Dot(box.Axis[1]),
        diff.Dot(box.Axis[2]));
    Vector3<Real> pmax = pmin;
    for (int i = 1; i < numPoints; ++i)
    {
        diff = points[i] - box.Center;
        for (int j = 0; j < 3; ++j)
        {
            Real dot = diff.Dot(box.Axis[j]);
            if (dot < pmin[j])
            {
                pmin[j] = dot;
            }
            else if (dot > pmax[j])
            {
                pmax[j] = dot;
            }
        }
    }

    box.Center +=
        (((Real)0.5)*(pmin[0] + pmax[0]))*box.Axis[0] +
        (((Real)0.5)*(pmin[1] + pmax[1]))*box.Axis[1] +
        (((Real)0.5)*(pmin[2] + pmax[2]))*box.Axis[2];

    box.Extent[0] = ((Real)0.5)*(pmax[0] - pmin[0]);
    box.Extent[1] = ((Real)0.5)*(pmax[1] - pmin[1]);
    box.Extent[2] = ((Real)0.5)*(pmax[2] - pmin[2]);

    return box;
}
//----------------------------------------------------------------------------
template <typename Real>
bool InBox (const Vector3<Real>& point, const Box3<Real>& box)
{
    Vector3<Real> diff = point - box.Center;
    for (int i = 0; i < 3; ++i)
    {
        Real coeff = diff.Dot(box.Axis[i]);
        if (Math<Real>::FAbs(coeff) > box.Extent[i])
        {
            return false;
        }
    }
    return true;
}
//----------------------------------------------------------------------------
template <typename Real>
Box3<Real> MergeBoxes (const Box3<Real>& box0, const Box3<Real>& box1)
{
    // Construct a box that contains the input boxes.
    Box3<Real> box;

    // The first guess at the box center.  This value will be updated later
    // after the input box vertices are projected onto axes determined by an
    // average of box axes.
    box.Center = ((Real)0.5)*(box0.Center + box1.Center);

    // A box's axes, when viewed as the columns of a matrix, form a rotation
    // matrix.  The input box axes are converted to quaternions.  The average
    // quaternion is computed, then normalized to unit length.  The result is
    // the slerp of the two input quaternions with t-value of 1/2.  The result
    // is converted back to a rotation matrix and its columns are selected as
    // the merged box axes.
    Quaternion<Real> q0, q1;
    q0.FromRotationMatrix(box0.Axis);
    q1.FromRotationMatrix(box1.Axis);
    if (q0.Dot(q1) < (Real)0)
    {
        q1 = -q1;
    }

    Quaternion<Real> q = q0 + q1;
    Real invLength = Math<Real>::InvSqrt(q.Dot(q));
    q = invLength*q;
    q.ToRotationMatrix(box.Axis);

    // Project the input box vertices onto the merged-box axes.  Each axis
    // D[i] containing the current center C has a minimum projected value
    // min[i] and a maximum projected value max[i].  The corresponding end
    // points on the axes are C+min[i]*D[i] and C+max[i]*D[i].  The point C
    // is not necessarily the midpoint for any of the intervals.  The actual
    // box center will be adjusted from C to a point C' that is the midpoint
    // of each interval,
    //   C' = C + sum_{i=0}^2 0.5*(min[i]+max[i])*D[i]
    // The box extents are
    //   e[i] = 0.5*(max[i]-min[i])

    int i, j;
    Real dot;
    Vector3<Real> vertex[8], diff;
    Vector3<Real> pmin = Vector3<Real>::ZERO;
    Vector3<Real> pmax = Vector3<Real>::ZERO;

    box0.ComputeVertices(vertex);
    for (i = 0; i < 8; ++i)
    {
        diff = vertex[i] - box.Center;
        for (j = 0; j < 3; ++j)
        {
            dot = diff.Dot(box.Axis[j]);
            if (dot > pmax[j])
            {
                pmax[j] = dot;
            }
            else if (dot < pmin[j])
            {
                pmin[j] = dot;
            }
        }
    }

    box1.ComputeVertices(vertex);
    for (i = 0; i < 8; ++i)
    {
        diff = vertex[i] - box.Center;
        for (j = 0; j < 3; ++j)
        {
            dot = diff.Dot(box.Axis[j]);
            if (dot > pmax[j])
            {
                pmax[j] = dot;
            }
            else if (dot < pmin[j])
            {
                pmin[j] = dot;
            }
        }
    }

    // [min,max] is the axis-aligned box in the coordinate system of the
    // merged box axes.  Update the current box center to be the center of
    // the new box.  Compute the extents based on the new center.
    for (j = 0; j < 3; ++j)
    {
        box.Center += (((Real)0.5)*(pmax[j] + pmin[j]))*box.Axis[j];
        box.Extent[j] = ((Real)0.5)*(pmax[j] - pmin[j]);
    }

    return box;
}
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Explicit instantiation.
//----------------------------------------------------------------------------
template WM5_MATHEMATICS_ITEM
Box3<float> ContAlignedBox<float> (int, const Vector3<float>*);

template WM5_MATHEMATICS_ITEM
Box3<float> ContOrientedBox<float> (int, const Vector3<float>*);

template WM5_MATHEMATICS_ITEM
bool InBox<float> (const Vector3<float>&, const Box3<float>&);

template WM5_MATHEMATICS_ITEM
Box3<float> MergeBoxes<float> (const Box3<float>&, const Box3<float>&);


template WM5_MATHEMATICS_ITEM
Box3<double> ContAlignedBox<double> (int, const Vector3<double>*);

template WM5_MATHEMATICS_ITEM
Box3<double> ContOrientedBox<double> (int, const Vector3<double>*);

template WM5_MATHEMATICS_ITEM
bool InBox<double> (const Vector3<double>&, const Box3<double>&);

template WM5_MATHEMATICS_ITEM
Box3<double> MergeBoxes<double> (const Box3<double>&, const Box3<double>&);
//----------------------------------------------------------------------------
}

template <typename Real>
void ComputeOriginal(int numPoints, const Wm5::Vector3<Real>* points, Wm5::Vector3<Real>& pmin, Wm5::Vector3<Real>& pmax){
    pmin = points[0];
    pmax = points[0];
    for (int i=0; i<numPoints; i++){
       pmin.X() = std::min(points[i].X(), pmin.X());
       pmin.Y() = std::min(points[i].Y(), pmin.Y());
       pmin.Z() = std::min(points[i].Z(), pmin.Z());

       pmax.X() = std::max(points[i].X(), pmax.X());
       pmax.Y() = std::max(points[i].Y(), pmax.Y());
       pmax.Z() = std::max(points[i].Z(), pmax.Z());
    }
}

template <typename Real>
Wm5::Box3<Real> ContOriginalBox(int numPoints, const Wm5::Vector3<Real>* points)
{
    Wm5::Vector3<Real> pmin, pmax;
    ComputeOriginal(numPoints, points, pmin, pmax);

    Wm5::Box3<Real> box;
    box.Center = ((Real)0.5)*(pmin + pmax);
    box.Axis[0] = Wm5::Vector3<Real>::UNIT_X;
    box.Axis[1] = Wm5::Vector3<Real>::UNIT_Y;
    box.Axis[2] = Wm5::Vector3<Real>::UNIT_Z;
    Wm5::Vector3<Real> halfDiagonal = ((Real)0.5)*(pmax - pmin);
    for (int i = 0; i < 3; ++i)
    {
        box.Extent[i] = halfDiagonal[i];
    }

    return box;
}
//----------------------------------------------------------------------------


int readHeader(std::ifstream& file){
    std::streampos beginData, triBeginData, endData, dataSize;

    file.seekg(0, std::ios::end);
    endData = file.tellg();
    file.seekg(0, std::ios::beg);
    beginData = file.tellg();

    MyStlHeader *header = new MyStlHeader;
    file >> *header;

    triBeginData = file.tellg();
    dataSize = endData - triBeginData;

std::cout << "dataSize" << dataSize << std::endl;
    if( dataSize % 50 != 0 ){
        std::cout << "Missing data in triangle" << std::endl;
        exit(1);
    }

    return header->numOfTriangles;
}

int readFile(std::ifstream& file, Wm5::Vector3<float>* vertices, const char* filename, const int numOfTriangles){

    for( int i=0;i<numOfTriangles;i++ ){
        MyStlTriangle *tri = new MyStlTriangle;
        file >> *tri;
        vertices[i*3+0] = Wm5::Vector3<float>(tri->vVertex1.x,tri->vVertex1.y,tri->vVertex1.z);
        vertices[i*3+1] = Wm5::Vector3<float>(tri->vVertex2.x,tri->vVertex2.y,tri->vVertex2.z);
        vertices[i*3+2] = Wm5::Vector3<float>(tri->vVertex3.x,tri->vVertex3.y,tri->vVertex3.z);
    }
    return numOfTriangles * 3;
}

void dumpResult(const char* title, const Wm5::Box3<float>* box){

    std::cout   << *title << ":" << std::endl
                << "box.Center:  " << box->Center  << std::endl
                << "box.Axis[0]: " << box->Axis[0] << std::endl
                << "box.Axis[1]: " << box->Axis[1] << std::endl
                << "box.Axis[2]: " << box->Axis[2] << std::endl
                << "box.Extent[0]: " << box->Extent[0] << std::endl
                << "box.Extent[1]: " << box->Extent[1] << std::endl
                << "box.Extent[2]: " << box->Extent[2] << std::endl;
}

void outputToFile(Json::Value& root, const std::string boxKind, const char* dataFilename, const Wm5::Box3<float>* box){
    root[dataFilename][boxKind]["boxkind"] = boxKind; 
    root[dataFilename][boxKind]["center"]["x"] = box->Center.X();
    root[dataFilename][boxKind]["center"]["y"] = box->Center.Y();
    root[dataFilename][boxKind]["center"]["z"] = box->Center.Z();
    root[dataFilename][boxKind]["axis0"]["a1"] = box->Axis[0].X();
    root[dataFilename][boxKind]["axis0"]["a2"] = box->Axis[0].Y();
    root[dataFilename][boxKind]["axis0"]["a3"] = box->Axis[0].Z();
    root[dataFilename][boxKind]["axis1"]["a1"] = box->Axis[1].X();
    root[dataFilename][boxKind]["axis1"]["a2"] = box->Axis[1].Y();
    root[dataFilename][boxKind]["axis1"]["a3"] = box->Axis[1].Z();
    root[dataFilename][boxKind]["axis2"]["a1"] = box->Axis[2].X();
    root[dataFilename][boxKind]["axis2"]["a2"] = box->Axis[2].Y();
    root[dataFilename][boxKind]["axis2"]["a3"] = box->Axis[2].Z();
    root[dataFilename][boxKind]["extent0"] = box->Extent[0];
    root[dataFilename][boxKind]["extent1"] = box->Extent[1];
    root[dataFilename][boxKind]["extent2"] = box->Extent[2];
}

// argv[1] STL filename
// argv[2] output tmp directory
int main(int argc, char**argv){
    
    // read stl file
    //std::cout << "argc = " << argc << std::endl;
    if( argc < 3 ){
        exit(1);
    }

    std::ifstream file(argv[1], std::ios::binary);
    int numOfTriangles = readHeader(file);

    Wm5::Vector3<float> *vertices = new Wm5::Vector3<float>[numOfTriangles*3];
    int numOfVertices = readFile(file, vertices, argv[1], numOfTriangles);

    // prepare JSON tree
    Json::Value root;
    root.clear();

    // pass values to analyzer
    Wm5::Box3<float> box = ContAlignedBox(numOfVertices, vertices);
    // dumpResult("Aligned", &box);
    outputToFile(root, "alignedBox", argv[1], &box);

    box = ContOriginalBox(numOfVertices, vertices);
    // dumpResult("Aligned", &box);
    outputToFile(root, "originalBox", argv[1], &box);

    box = ContOrientedBox(numOfVertices, vertices);
    // dumpResult("Oriented", &box)
    outputToFile(root, "orientedBox", argv[1], &box);

    std::ofstream ofs;

    char szJsonOutDir[_MAX_PATH];
    char baseFilename[_MAX_PATH];
    strcpy(baseFilename, basename( argv[1] ));

    strcpy(szJsonOutDir, argv[2]);
    strcat(szJsonOutDir, "/StlAnalyzerOut_");
    strcat(szJsonOutDir, baseFilename);
    strcat(szJsonOutDir, ".json");

    ofs.open(szJsonOutDir);
    ofs << root;
    ofs.close();
}

