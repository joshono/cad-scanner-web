#include <fstream>
#include <stdint.h>

struct MyVertex { float x, y, z; };

#define STL_HEADER_SIZE 80
struct MyStlTriangle{
    MyVertex          vNormal;
    MyVertex          vVertex1;
    MyVertex          vVertex2;
    MyVertex          vVertex3;
    uint16_t        attrByteCount;
};

struct MyStlHeader{
    uint8_t  header [STL_HEADER_SIZE];
    uint32_t numOfTriangles;
};

//std::ostream& operator<<(std::ostream& stream, const Triangle &info);
//std::ofstream& operator<<(std::ofstream& stream, Triangle &info);
std::ifstream& operator>>(std::ifstream& stream, MyStlTriangle &info);
std::ifstream& operator>>(std::ifstream& stream, MyStlHeader &info);

std::ifstream& operator>>(std::ifstream& stream, MyStlHeader &info)
{
    stream.read(reinterpret_cast<char*>(&info.header), sizeof(uint8_t)*STL_HEADER_SIZE);
    stream.read(reinterpret_cast<char*>(&info.numOfTriangles), sizeof(uint32_t));
    return stream;
}

std::ifstream& operator>>(std::ifstream& stream, MyStlTriangle &info)
{
    stream.read(reinterpret_cast<char*>(&info.vNormal.x), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.vNormal.y), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.vNormal.z), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.vVertex1.x), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.vVertex1.y), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.vVertex1.z), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.vVertex2.x), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.vVertex2.y), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.vVertex2.z), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.vVertex3.x), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.vVertex3.y), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.vVertex3.z), sizeof(float));
    stream.read(reinterpret_cast<char*>(&info.attrByteCount), sizeof(uint16_t));
    return stream;
}


