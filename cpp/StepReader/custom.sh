#!/bin/bash

#Define CASROOT and QTDIR variables in order to generate Makefile files by qmake.
#If you are using Mac OS X it is necessary to define WOKHOME as a path to your WOK installation directory.

export CASROOT="/opt/OCCT"

export QTDIR="/opt/Qt/5.2.0/gcc_64"

export WOKHOME=""
