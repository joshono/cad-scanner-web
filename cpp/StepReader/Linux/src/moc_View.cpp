/****************************************************************************
** Meta object code from reading C++ file 'View.h'
**
** Created: Thu Jan 16 10:32:07 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../Common/src/View.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'View.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_View[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
       6,    5,    5,    5, 0x05,

 // slots: signature, parameters, type, tag, flags
      25,    5,    5,    5, 0x0a,
      34,    5,    5,    5, 0x0a,
      44,    5,    5,    5, 0x0a,
      51,    5,    5,    5, 0x0a,
      57,    5,    5,    5, 0x0a,
      69,    5,    5,    5, 0x0a,
      77,    5,    5,    5, 0x0a,
      84,    5,    5,    5, 0x0a,
      90,    5,    5,    5, 0x0a,
      99,    5,    5,    5, 0x0a,
     106,    5,    5,    5, 0x0a,
     114,    5,    5,    5, 0x0a,
     120,    5,    5,    5, 0x0a,
     131,    5,    5,    5, 0x0a,
     139,    5,    5,    5, 0x0a,
     147,    5,    5,    5, 0x0a,
     156,    5,    5,    5, 0x0a,
     176,    5,    5,    5, 0x0a,
     191,    5,    5,    5, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_View[] = {
    "View\0\0selectionChanged()\0fitAll()\0"
    "fitArea()\0zoom()\0pan()\0globalPan()\0"
    "front()\0back()\0top()\0bottom()\0left()\0"
    "right()\0axo()\0rotation()\0reset()\0"
    "hlrOn()\0hlrOff()\0updateToggled(bool)\0"
    "onBackground()\0onEnvironmentMap()\0"
};

const QMetaObject View::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_View,
      qt_meta_data_View, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &View::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *View::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *View::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_View))
        return static_cast<void*>(const_cast< View*>(this));
    return QWidget::qt_metacast(_clname);
}

int View::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: selectionChanged(); break;
        case 1: fitAll(); break;
        case 2: fitArea(); break;
        case 3: zoom(); break;
        case 4: pan(); break;
        case 5: globalPan(); break;
        case 6: front(); break;
        case 7: back(); break;
        case 8: top(); break;
        case 9: bottom(); break;
        case 10: left(); break;
        case 11: right(); break;
        case 12: axo(); break;
        case 13: rotation(); break;
        case 14: reset(); break;
        case 15: hlrOn(); break;
        case 16: hlrOff(); break;
        case 17: updateToggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: onBackground(); break;
        case 19: onEnvironmentMap(); break;
        default: ;
        }
        _id -= 20;
    }
    return _id;
}

// SIGNAL 0
void View::selectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
