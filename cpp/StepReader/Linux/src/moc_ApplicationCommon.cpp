/****************************************************************************
** Meta object code from reading C++ file 'ApplicationCommon.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../../opt/OCCT/samples/qt/Common/src/ApplicationCommon.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ApplicationCommon.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ApplicationCommonWindow_t {
    QByteArrayData data[23];
    char stringdata[301];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_ApplicationCommonWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_ApplicationCommonWindow_t qt_meta_stringdata_ApplicationCommonWindow = {
    {
QT_MOC_LITERAL(0, 0, 23),
QT_MOC_LITERAL(1, 24, 8),
QT_MOC_LITERAL(2, 33, 15),
QT_MOC_LITERAL(3, 49, 0),
QT_MOC_LITERAL(4, 50, 10),
QT_MOC_LITERAL(5, 61, 13),
QT_MOC_LITERAL(6, 75, 8),
QT_MOC_LITERAL(7, 84, 15),
QT_MOC_LITERAL(8, 100, 6),
QT_MOC_LITERAL(9, 107, 18),
QT_MOC_LITERAL(10, 126, 7),
QT_MOC_LITERAL(11, 134, 13),
QT_MOC_LITERAL(12, 148, 15),
QT_MOC_LITERAL(13, 164, 12),
QT_MOC_LITERAL(14, 177, 15),
QT_MOC_LITERAL(15, 193, 17),
QT_MOC_LITERAL(16, 211, 8),
QT_MOC_LITERAL(17, 220, 1),
QT_MOC_LITERAL(18, 222, 22),
QT_MOC_LITERAL(19, 245, 20),
QT_MOC_LITERAL(20, 266, 7),
QT_MOC_LITERAL(21, 274, 13),
QT_MOC_LITERAL(22, 288, 11)
    },
    "ApplicationCommonWindow\0onNewDoc\0"
    "DocumentCommon*\0\0onNewDocRT\0onCloseWindow\0"
    "onUseVBO\0onCloseDocument\0theDoc\0"
    "onSelectionChanged\0onAbout\0onViewToolBar\0"
    "onViewStatusBar\0onToolAction\0"
    "onCreateNewView\0onWindowActivated\0"
    "QWidget*\0w\0windowsMenuAboutToShow\0"
    "windowsMenuActivated\0checked\0onSetMaterial\0"
    "theMaterial\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ApplicationCommonWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   89,    3, 0x0a,
       4,    0,   90,    3, 0x0a,
       5,    0,   91,    3, 0x0a,
       6,    0,   92,    3, 0x0a,
       7,    1,   93,    3, 0x0a,
       9,    0,   96,    3, 0x0a,
      10,    0,   97,    3, 0x0a,
      11,    0,   98,    3, 0x0a,
      12,    0,   99,    3, 0x0a,
      13,    0,  100,    3, 0x0a,
      14,    0,  101,    3, 0x0a,
      15,    1,  102,    3, 0x0a,
      18,    0,  105,    3, 0x0a,
      19,    1,  106,    3, 0x0a,
      21,    1,  109,    3, 0x0a,

 // slots: parameters
    0x80000000 | 2,
    0x80000000 | 2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 2,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 16,   17,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   20,
    QMetaType::Void, QMetaType::Int,   22,

       0        // eod
};

void ApplicationCommonWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ApplicationCommonWindow *_t = static_cast<ApplicationCommonWindow *>(_o);
        switch (_id) {
        case 0: { DocumentCommon* _r = _t->onNewDoc();
            if (_a[0]) *reinterpret_cast< DocumentCommon**>(_a[0]) = _r; }  break;
        case 1: { DocumentCommon* _r = _t->onNewDocRT();
            if (_a[0]) *reinterpret_cast< DocumentCommon**>(_a[0]) = _r; }  break;
        case 2: _t->onCloseWindow(); break;
        case 3: _t->onUseVBO(); break;
        case 4: _t->onCloseDocument((*reinterpret_cast< DocumentCommon*(*)>(_a[1]))); break;
        case 5: _t->onSelectionChanged(); break;
        case 6: _t->onAbout(); break;
        case 7: _t->onViewToolBar(); break;
        case 8: _t->onViewStatusBar(); break;
        case 9: _t->onToolAction(); break;
        case 10: _t->onCreateNewView(); break;
        case 11: _t->onWindowActivated((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 12: _t->windowsMenuAboutToShow(); break;
        case 13: _t->windowsMenuActivated((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->onSetMaterial((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< DocumentCommon* >(); break;
            }
            break;
        }
    }
}

const QMetaObject ApplicationCommonWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ApplicationCommonWindow.data,
      qt_meta_data_ApplicationCommonWindow,  qt_static_metacall, 0, 0}
};


const QMetaObject *ApplicationCommonWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ApplicationCommonWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ApplicationCommonWindow.stringdata))
        return static_cast<void*>(const_cast< ApplicationCommonWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ApplicationCommonWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
