/****************************************************************************
** Meta object code from reading C++ file 'Transparency.h'
**
** Created: Thu Jan 16 10:32:06 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../Common/src/Transparency.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Transparency.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DialogTransparency[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      26,   20,   19,   19, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_DialogTransparency[] = {
    "DialogTransparency\0\0value\0"
    "sendTransparencyChanged(int)\0"
};

const QMetaObject DialogTransparency::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_DialogTransparency,
      qt_meta_data_DialogTransparency, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DialogTransparency::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DialogTransparency::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DialogTransparency::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DialogTransparency))
        return static_cast<void*>(const_cast< DialogTransparency*>(this));
    return QDialog::qt_metacast(_clname);
}

int DialogTransparency::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: sendTransparencyChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void DialogTransparency::sendTransparencyChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
