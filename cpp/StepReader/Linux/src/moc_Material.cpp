/****************************************************************************
** Meta object code from reading C++ file 'Material.h'
**
** Created: Thu Jan 16 10:32:05 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../Common/src/Material.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Material.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DialogMaterial[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      46,   41,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DialogMaterial[] = {
    "DialogMaterial\0\0sendMaterialChanged(int)\0"
    "isOn\0updateButtons(bool)\0"
};

const QMetaObject DialogMaterial::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_DialogMaterial,
      qt_meta_data_DialogMaterial, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DialogMaterial::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DialogMaterial::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DialogMaterial::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DialogMaterial))
        return static_cast<void*>(const_cast< DialogMaterial*>(this));
    return QDialog::qt_metacast(_clname);
}

int DialogMaterial::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: sendMaterialChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: updateButtons((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void DialogMaterial::sendMaterialChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
