// STEPReader.cpp :

// TODO:
// user const declaration, macro replaced with template inline
// replace stdio.h with iostream : type safety


#include <GL/glew.h>
#include <glut.h>
//#include <GL/glx.h>

// QT
#define QT_CLEAN_NAMESPACE         /* avoid definition of INT32 and INT8 */
#include <QDataStream>
#include <QtWidgets/QApplication>
#include <QPainter>
#include <QtWidgets/QMenu>
#include <QtWidgets/QColorDialog>
#include <QCursor>
#include <QFileInfo>
#include <QtWidgets/QFileDialog>
#include <QMouseEvent>
#include <QtWidgets/QRubberBand>
//#include <QWindowsStyle>
#include <QtX11Extras/QX11Info>
#include <QtWidgets/QColormap>
#include <QtWidgets/QWidget>
#include <QGLWidget>
#include <QWindow>

// C++ STL
#include <vector>
#include <list>
#include <iostream>
#include <string>

// OCC
#include <TopoDS.hxx>
#include <AIS_Shape.hxx>
#include <AIS_InteractiveContext.hxx>
#include <Graphic3d.hxx>
#include <Visual3d_View.hxx>
#include <STEPControl_Reader.hxx>
#include <STEPControl_StepModelType.hxx>
#include <STEPControl_Writer.hxx>
#include <TopTools_HSequenceOfShape.hxx>
#include <TopTools_SequenceOfShape.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Compound.hxx>
#include <TopoDS_Iterator.hxx>
#include <BRep_Builder.hxx>
#include <StlAPI_Writer.hxx>
#include <V3d_Viewer.hxx>
#include <V3d_View.hxx>
#include <V3d_DirectionalLight.hxx>
#include <V3d_AmbientLight.hxx>
#include <Interface_Static.hxx>
#include <Prs3d_ShapeTool.hxx>
#include <GProp_GProps.hxx>
#include <BRepGProp.hxx>
#include <BRepBndLib.hxx>
#include <BRepLib.hxx>
#include <Image_AlienPixMap.hxx>
#include <Image_PixMap.hxx>
#include <Xw_Window.hxx>
#include <OpenGl_GraphicDriver.hxx>

// GL/X11
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/Xmu/StdCmap.h>
#include <X11/Xlib.h>

// C/Linux
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <libgen.h>
#include <strings.h>

//jsoncpp
#include <json/json.h>

//using namespace Gdiplus;
using namespace std;

// OpenCASCADE
#define OCC_VOLUME

#define _MAX_PATH           1024

// FIXME: should be windows param
#define _MAX_EXT            100
#define _MAX_DIR            1024
#define _MAX_DRIVE          10

// window
#define WINDOW_WIDTH        300
#define WINDOW_HEIGHT       300
#define WINDOW_POSITION_X   0
#define WINDOW_POSITION_Y   0

// global variables
char	gszDir[_MAX_PATH];			// 
char	gszOutDir[_MAX_PATH];		//
char	gsztmpDir[_MAX_PATH];		//
char	gszFileName[_MAX_PATH];		// 
char	gszExt[_MAX_PATH];			// 
int		giViewOption;				// VIEW OPTION
int		giRed;
int		giGreen;
int		giBlue;
int		giRemoveCurves = 1;
double	gVolume = 0;				//
double	gSurfaceArea = 0;			//
unsigned int gSurfaceNum = 0;

std::pair<int,int>	TextureWH( 512, 512 );
unsigned int	texID[1];
unsigned int	FboID[1];			// 
unsigned int	RboID[1];			// 

// View Option
enum ViewOptionValue { 
	ViewOption_Xpos,				// +X
	ViewOption_Xneg,				// -X
	ViewOption_Ypos,				// +Y
	ViewOption_Yneg,				// -Y
	ViewOption_Zpos,				// +Z
	ViewOption_Zneg,				// -Z
	ViewOption_XposYnegZpos,		// +ISOMETRIC
	ViewOption_XnegYposZneg			// -ISOMETRIC
};

#define	VIEWOPTION_XPOS				"+X"
#define	VIEWOPTION_XNEG				"-X"
#define	VIEWOPTION_YPOS				"+Y"
#define	VIEWOPTION_YNEG				"-Y"
#define	VIEWOPTION_ZPOS				"+Z"
#define	VIEWOPTION_ZNEG				"-Z"
#define	VIEWOPTION_XPOSYNEGZPOS		"+ISOMETRIC"
#define	VIEWOPTION_XNEGYPOSZNEG		"-ISOMETRIC"

#define READER_FILE_STL				"STLAnalyzer.exe"

//
#define FILE_EXT_BMP				".bmp"
#define FILE_EXT_JPEG				".jpeg"
#define FILE_EXT_STL				".stl"
#define FILE_EXT_TMP				".tmp"

// Exit Code
enum ConvertExitInfo {
	ConvertExitInfo_Success,		// Success
	ConvertExitInfo_CantRead_ImportStep,		// Read Error
	ConvertExitInfo_CantRead_AnalysisDummy,		// Read Error
	ConvertExitInfo_CantRead_ExportStl,		// Read Error
	ConvertExitInfo_CantCreate,		// Create Error
	ConvertExitInfo_Unexpected,		// UNEXPECTED
	ConvertExitInfo_Model_Reg		// Model_Registration
};

#define CONVERT_EXIT_INFO_SUCCESS		""
#define CONVERT_EXIT_INFO_CANTREAD_IMPORTSTEP		"CAN'T READ, IMPORT STEP"
#define CONVERT_EXIT_INFO_CANTREAD_ANALYSISDUMMY	"CAN'T READ, NO ANALYSIS"
#define CONVERT_EXIT_INFO_CANTREAD_EXPORTSTL		"CAN'T READ, EXPORT STL"
#define CONVERT_EXIT_INFO_CANTCREATE	"CAN'T CREATE"
#define CONVERT_EXIT_INFO_UNEXPECTED	"UNEXPECTED"
#define CONVERT_EXIT_INFO_MODEL_REG		"MODEL REGISTRATION FAILURE"

Handle(V3d_Viewer) m_myViewer;
Handle(V3d_View) m_myView;
Handle(AIS_InteractiveContext) m_myAISContext;
QWindow *m_widget;
QApplication *m_app;
Aspect_DisplayConnection *m_DisplayConnection;
Handle(OpenGl_GraphicDriver) m_theGraphicDriver;

Standard_CString m_Unit;
double m_dFacsimileArea;
double m_dFacsimileAreaX;
double m_dFacsimileAreaY;
double m_dFacsimileAreaZ;

int	m_iLicenseCode;			// 
							// 0 :
							// 1 :

typedef unsigned long DWORD;

/////////////////////////////////////////////
//
/////////////////////////////////////////////
const char *GetExitCode(DWORD dwExCode)
{
	// Exit Code
	if( dwExCode == ConvertExitInfo_Success ) {
		return CONVERT_EXIT_INFO_SUCCESS;
	} else if( dwExCode == ConvertExitInfo_CantRead_ImportStep ) {
		return CONVERT_EXIT_INFO_CANTREAD_IMPORTSTEP;
	} else if( dwExCode == ConvertExitInfo_CantRead_AnalysisDummy ) {
		return CONVERT_EXIT_INFO_CANTREAD_ANALYSISDUMMY;
	} else if( dwExCode == ConvertExitInfo_CantRead_ExportStl ) {
		return CONVERT_EXIT_INFO_CANTREAD_EXPORTSTL;
	} else if( dwExCode == ConvertExitInfo_CantCreate ) {
		return CONVERT_EXIT_INFO_CANTCREATE;
	} else if( dwExCode == ConvertExitInfo_Unexpected ) {
		return CONVERT_EXIT_INFO_UNEXPECTED;
	} else if( dwExCode == ConvertExitInfo_Model_Reg ) {
		return CONVERT_EXIT_INFO_MODEL_REG;
	} else {
		return CONVERT_EXIT_INFO_UNEXPECTED;
	}

	return NULL;
}

/////////////////////////////////////////////
// VIEW OPTION 
/////////////////////////////////////////////
const char *GetViewOption(int iViewOption)
{
	// VIEW OPTION
	if( iViewOption == ViewOption_Xpos ) {					// +X
		return VIEWOPTION_XPOS;
	} else if( iViewOption == ViewOption_Xneg ) {			// -X
		return VIEWOPTION_XNEG;
	} else if( iViewOption == ViewOption_Ypos ) {			// +Y
		return VIEWOPTION_YPOS;
	} else if( iViewOption == ViewOption_Yneg ) {			// -Y
		return VIEWOPTION_YNEG;
	} else if( iViewOption == ViewOption_Zpos ) {			// +Z
		return VIEWOPTION_ZPOS;
	} else if( iViewOption == ViewOption_Zneg ) {			// -Z
		return VIEWOPTION_ZNEG;
	} else if( iViewOption == ViewOption_XposYnegZpos ) {	// +ISOMETRIC
		return VIEWOPTION_XPOSYNEGZPOS;
	} else if( iViewOption == ViewOption_XnegYposZneg ) {	// -ISOMETRIC
		return VIEWOPTION_XNEGYPOSZNEG;
	}

	return NULL;
}

/////////////////////////////////////////////
//
/////////////////////////////////////////////
vector<string> split(string str, string delim)
{
	vector<string> result;
	int cutAt;
	while( (cutAt = str.find_first_of(delim)) != str.npos ) {
		if( cutAt > 0 ) {
			result.push_back( str.substr(0, cutAt) );
		}
		str = str.substr( cutAt + 1 );
	}
	if( str.length() > 0 ) {
		result.push_back( str );
	}
	return result;
}

/////////////////////////////////////////////
// 
/////////////////////////////////////////////
char *StrToLower(char *s)
{
	char *p;
	for (p = s; *p; p++)
		*p = tolower(*p);
	return (s);
}

/////////////////////////////////////////////
//
/////////////////////////////////////////////
bool TempReportFile(char *lpszInPath, char *lpszFileName, char *lpszExt, int iCode, 
	char *lpszUnit, double dFacsimileArea, int iViewOption, vector<string> vAnalysisInfo, unsigned long ulSurfaceNum, double shape_volume)
{
cout << "TempReportFile, inPath/filename=" << lpszInPath << "/" << lpszFileName << endl;
	try {
		FILE	*fp;
		int	    error;
		char	szUnitPath[_MAX_PATH];		//
		char	szInputFile[256];			//
		char	szOutputFile[256];			// JPEG
		char	szViewOption[256];			// VIEW OPTION
		char	szExitInfo[256];			// JPEG
		double	dVolume = 0;				//
		double	dSurfaceArea = 0;			//

		//
		memset( szUnitPath, 0x00, sizeof(szUnitPath) );
		memset( szInputFile, 0x00, sizeof(szInputFile) );
		memset( szOutputFile, 0x00, sizeof(szOutputFile) );
		memset( szViewOption, 0x00, sizeof(szViewOption) );
		memset( szExitInfo, 0x00, sizeof(szExitInfo) );

cout << "TempReportFile, try: strcpy" << endl;
		strcpy( szUnitPath, gsztmpDir );
		strcat( szUnitPath, "/Report_" );
		strcat( szUnitPath, lpszFileName );
		strcat( szUnitPath, lpszExt );
		strcat( szUnitPath, FILE_EXT_TMP );

cout << "TempReportFile, szunitPath=" << szUnitPath << endl;
		// Open
		if ( (fp = fopen(szUnitPath ,"a")) == 0 ) {
cout << "TempReportFile, fopen failed" << endl;
			return false;
		}

		// 
		strcpy( szInputFile, lpszFileName );
		strcat( szInputFile, lpszExt );

		// Exit Code
		strcpy( szExitInfo, GetExitCode(iCode) );

cout << "TempReportFile, try: write" << endl;
		// Write
		if( iCode == ConvertExitInfo_Success ) {
			// 
			string strSurfArea = vAnalysisInfo[0].c_str();
			istringstream isSurf;
			isSurf.str(strSurfArea);
			isSurf >> dSurfaceArea;
			if( dSurfaceArea < 0 ) {
				// 
				dSurfaceArea = 0;
			}

			// 
#ifdef OCC_VOLUME
			dVolume = shape_volume;
#else
cout << "TempReportFile, try: ifndef OCC_VOLUME" << endl;
			string strVol = vAnalysisInfo[1].c_str();
			istringstream isVol;
			isVol.str(strVol);
			isVol >> dVolume;
#endif
			if( dVolume < 0 ) {
				// 
				dVolume = 0;
			}
cout << "TempReportFile, try: vBox" << endl;

			vector<string> vBox = split( vAnalysisInfo[8].c_str(), "," );
			vector<string> vGrv = split( vAnalysisInfo[2].c_str(), "," );

			// VIEW OPTION
			strcpy( szViewOption, GetViewOption(iViewOption) );

			// JPEG
			strcpy( szOutputFile, gszFileName );
			strcat( szOutputFile, "_" );
			strcat( szOutputFile, szViewOption );
			strcat( szOutputFile, FILE_EXT_JPEG );

            gVolume = dVolume;
            gSurfaceArea = dSurfaceArea;
			if( dFacsimileArea == 0 ) {
cout << "TempReportFile, facsimileArea=0" << endl;
				fprintf( fp, "%s,%s,%s,'%s,%s,%s,%s,%s,%s,%f,%f,%s,%s,%s,%s,%ld\n",
					lpszInPath,						// INPUT PATH
					szInputFile,					// 
					szExitInfo,						// 
					szViewOption,					// VIEW OPTION
					szOutputFile,					// JPEG
					StrToLower(lpszUnit),			// 
					vBox[0].c_str(),				//
					vBox[1].c_str(),				// 
					vBox[2].c_str(),				// 
					dSurfaceArea,					// 
					dVolume,						// 
					vGrv[0].c_str(),				// 
					vGrv[1].c_str(),				// 
					vGrv[2].c_str(),				// 
					"",								// 
					ulSurfaceNum );					// 
			} else {
cout << "TempReportFile, facsimileArea!=0" << endl;
				fprintf( fp, "%s,%s,%s,'%s,%s,%s,%s,%s,%s,%f,%f,%s,%s,%s,%f,%ld\n",
					lpszInPath,						// INPUT PATH
					szInputFile,					// 
					szExitInfo,						// 
					szViewOption,					// VIEW OPTION
					szOutputFile,					// JPEG
					StrToLower(lpszUnit),			// 
					vBox[0].c_str(),				// 
					vBox[1].c_str(),				// 
					vBox[2].c_str(),				// 
					dSurfaceArea,					// 
					dVolume,						// 
					vGrv[0].c_str(),				// 
					vGrv[1].c_str(),				// 
					vGrv[2].c_str(),				// 
					dFacsimileArea,					// 
					ulSurfaceNum );					// 
			}
		} else {
cout << "TempReportFile, exception occurred" << endl;
cout << "TempReportFile, inpath=" << lpszInPath << endl;
cout << "TempReportFile, inputfile=" << szInputFile << endl;
cout << "TempReportFile, exitinfo=" << szExitInfo << endl;
			fprintf( fp, "%s,%s,%s\n",
				lpszInPath,						// INPUT PATH
				szInputFile,					//
				szExitInfo );					//
		}

		// Close
		fclose( fp );

	}
	catch( ... ) {
		exit( ConvertExitInfo_Unexpected );
	}

	return true;
}

/////////////////////////////////////////////
// BMP  JPEG 
/////////////////////////////////////////////
bool JpegConvert( Handle(V3d_View) myView )
{
cout << "JpegConvert: entering" << endl;
	try {
		char	szBMPPath[_MAX_PATH];		// BMP
		char	szJPGPath[_MAX_PATH];		// JPEG

		// 
		memset( szBMPPath, 0x00, sizeof(szBMPPath) );
		memset( szJPGPath, 0x00, sizeof(szJPGPath) );

		strcpy( szBMPPath, gszOutDir );
		strcat( szBMPPath, gszFileName );
		strcat( szBMPPath, "_" );

		// VIEW OPTION
		switch( giViewOption ) {
		case ViewOption_Xpos:			// +X
			strcat( szBMPPath, VIEWOPTION_XPOS );
			break;
		case ViewOption_Xneg:			// -X
			strcat( szBMPPath, VIEWOPTION_XNEG );
			break;
		case ViewOption_Ypos:			// +Y
			strcat( szBMPPath, VIEWOPTION_YPOS );
			break;
		case ViewOption_Yneg:			// -Y
			strcat( szBMPPath, VIEWOPTION_YNEG );
			break;
		case ViewOption_Zpos:			// +Z
			strcat( szBMPPath, VIEWOPTION_ZPOS );
			break;
		case ViewOption_Zneg:			// -Z
			strcat( szBMPPath, VIEWOPTION_ZNEG );
			break;
		case ViewOption_XposYnegZpos:	// +ISOMETRIC
			strcat( szBMPPath, VIEWOPTION_XPOSYNEGZPOS );
			break;
		case ViewOption_XnegYposZneg:	// -ISOMETRIC
			strcat( szBMPPath, VIEWOPTION_XNEGYPOSZNEG );
			break;
		default:
			return false;
		}

cout << "JpegConvert: szBMPPath=" << szBMPPath << endl;

		// Jpeg
		strcpy( szJPGPath, szBMPPath );
		strcat( szJPGPath, FILE_EXT_JPEG );

		// BMP
		strcat( szBMPPath, FILE_EXT_BMP );

		// BMP
		//Handle(Image_AlienPixMap) aPixMap;
		//Image_AlienPixMap *aPixMap = new Image_AlienPixMap();
        //myView->ToPixMap( *aPixMap, WINDOW_WIDTH, WINDOW_HEIGHT );
		//aPixMap->Save( szBMPPath );
        myView->Dump( szJPGPath );

	}
	catch( ... ) {
		exit( ConvertExitInfo_Unexpected );
	}

	return true;
}

/////////////////////////////////////////////
// STL
/////////////////////////////////////////////
bool exportSTL( const TopoDS_Shape& shapes )
{
	try {

		char	szSTLPath[_MAX_PATH];		// STL

		// 
		memset( szSTLPath, 0x00, sizeof(szSTLPath) );

		strcpy( szSTLPath, gsztmpDir );
		strcat( szSTLPath, "/" );
		strcat( szSTLPath, gszFileName );
		strcat( szSTLPath, FILE_EXT_STL );
		//strcpy( szSTLPath, "/tmp/sample.stl");

		StlAPI_Writer writer;
		writer.ASCIIMode() = Standard_False;
		writer.SetCoefficient( 10 );
		writer.Write( shapes, szSTLPath);
	}
	catch( ... ) {
		exit( ConvertExitInfo_Unexpected );
	}

	return true;
}

bool exploreCompound(BRep_Builder& builder, TopoDS_Compound& comp_out, TopoDS_Shape& comp_in)
{
	try
	{
		TopoDS_Compound comp = TopoDS::Compound(comp_in);
		for(TopoDS_Iterator it(comp); it.More(); it.Next())
		{
			TopoDS_Shape comp_shape = it.Value();
			if( TopAbs_SOLID == comp_shape.ShapeType() ||
				TopAbs_SHELL == comp_shape.ShapeType() ||
				TopAbs_FACE == comp_shape.ShapeType())
			{
				builder.Add(comp_out, comp_shape);
			}
			else if(TopAbs_COMPOUND == comp_shape.ShapeType())
			{
				exploreCompound(builder, comp_out, comp_shape);
			}
		}
	}
	catch (...)
	{
		return false;
	}


	return true;
}

/////////////////////////////////////////////
//
/////////////////////////////////////////////
bool importSTEP(TopoDS_Compound& compound)
{
	BRep_Builder builder;
	builder.MakeCompound(compound);

	Handle(TopTools_HSequenceOfShape) aSequence;

	try {
		STEPControl_Reader reader;

		// STEP File Read
		IFSelect_ReturnStatus stat = reader.ReadFile( gszDir );
		if( stat != IFSelect_RetDone ) {
			// STEP File Read Error!!
			return false;
		}

		/*
		TopAbs_COMPOUND,
		TopAbs_COMPSOLID,
		TopAbs_SOLID,
		TopAbs_SHELL,
		TopAbs_FACE,
		TopAbs_WIRE,
		TopAbs_EDGE,
		TopAbs_VERTEX,
		TopAbs_SHAPE
		*/
		reader.ClearShapes();
		if(giRemoveCurves)
		{
			Standard_Integer shape_count = reader.NbRootsForTransfer();
			if( shape_count == 0 ) {
				return false;
			}

			reader.TransferRoots();
			shape_count = reader.NbShapes();
			for(Standard_Integer i = 1; i <= shape_count; ++i)
			{
				TopoDS_Shape shape = reader.Shape(i);
				if(shape.IsNull())
				{
					continue;
				}

				if( TopAbs_SOLID == shape.ShapeType() ||
					TopAbs_COMPSOLID == shape.ShapeType() ||
					TopAbs_SHELL == shape.ShapeType() ||
					TopAbs_FACE == shape.ShapeType())
				{
					builder.Add(compound, shape);
				}
				else if(TopAbs_COMPOUND == shape.ShapeType())
				{
					if(!exploreCompound(builder, compound, shape))
					{
						return false;
					}
					//TopoDS_Compound comp = TopoDS::Compound(shape);
					//for(TopoDS_Iterator it(comp); it.More(); it.Next())
					//{
					//	const TopoDS_Shape& comp_shape = it.Value();
					//	if( TopAbs_SOLID == comp_shape.ShapeType() ||
					//		TopAbs_SHELL == comp_shape.ShapeType() ||
					//		TopAbs_FACE == comp_shape.ShapeType())
					//	{
					//		builder.Add(compound, comp_shape);
					//	}
					//	else
					//	{
					//		TopAbs_ShapeEnum comp_shape_type = comp_shape.ShapeType();
					//		int i = 5;
					//	}
					//}
				}
			}
			//for(int i = 1; i <= num_roots; ++i)
			//{
			//	Handle(Standard_Transient) root = reader.RootForTransfer(i);
			//	reader.TransferEntity(root);
			//	TopoDS_Shape shape = reader.Shape(i);
			//	TopAbs_ShapeEnum shape_type = shape.ShapeType();

			//	// SHELL  SOLID
			//	if(	TopAbs_COMPSOLID == shape_type ||
			//		TopAbs_SOLID == shape_type ||
			//		TopAbs_SHELL == shape_type)
			//	{
			//		builder.Add(compound, shape);
			//	}
			//	// WIRE, EDGE, VERTEX  FACE
			//	else
			//	{
			//		//for(TopExp_Explorer explorer(shape, TopAbs_SHELL); explorer.More(); explorer.Next())
			//		//{
			//		//	builder.Add(compound, explorer.Current());
			//		//}

			//		for (TopExp_Explorer explorer(shape, TopAbs_FACE); explorer.More(); explorer.Next())
			//		{
			//			builder.Add(compound, explorer.Current());
			//		}
			//	}
			//}
		}
		else
		{
			reader.TransferRoots();
			TopoDS_Shape sh = reader.OneShape();
			if( sh.IsNull() ) {
				return false;
			}
			builder.Add(compound, sh);
		}

	}
	catch( ... ) {
		exit( ConvertExitInfo_Unexpected );
	}

	return true;
}

/////////////////////////////////////////////
//
/////////////////////////////////////////////
int display()
{
cout << "diaplay() entering" << endl;
	try {
		glClear( GL_COLOR_BUFFER_BIT );

		// View Option
		switch( giViewOption ) {
		case ViewOption_Xpos:			// +X
			m_myView->SetProj( V3d_Xpos );
			break;
		case ViewOption_Xneg:			// -X
			m_myView->SetProj( V3d_Xneg );
			break;
		case ViewOption_Ypos:			// +Y
			m_myView->SetProj( V3d_Ypos );
			break;
		case ViewOption_Yneg:			// -Y
			m_myView->SetProj( V3d_Yneg );
			break;
		case ViewOption_Zpos:			// +Z
			m_myView->SetProj( V3d_Zpos );
			break;
		case ViewOption_Zneg:			// -Z
			m_myView->SetProj( V3d_Zneg );
			break;
		case ViewOption_XposYnegZpos:	// +ISOMETRIC
			m_myView->SetProj( V3d_XposYnegZpos );
			break;
		case ViewOption_XnegYposZneg:	// -ISOMETRIC
			m_myView->SetProj( V3d_XnegYposZneg );
			break;
		default:
			return -1;
		}

		m_myView->FitAll();
		m_myView->ZFitAll();

		glFinish();

		//////////////////////////
		// Output File
		//////////////////////////
		JpegConvert( m_myView );

	}
	catch( ... ) {
		return -1;
	}

	return ConvertExitInfo_Success;
}

/////////////////////////////////////////////
// 
/////////////////////////////////////////////
int projArea(const vector<string>& vAnalysisInfo)
{
	try {
		vector<string> vProjArea = split(vAnalysisInfo[9].c_str(), ",");

		// View Option
		switch( giViewOption ) {
		case ViewOption_Xpos:			// +X
			break;
		case ViewOption_Xneg:			// -X
			m_dFacsimileArea = m_dFacsimileAreaX;
			break;
		case ViewOption_Ypos:			// +Y
			break;
		case ViewOption_Yneg:			// -Y
			m_dFacsimileArea = m_dFacsimileAreaY;
			break;
		case ViewOption_Zpos:			// +Z
			break;
		case ViewOption_Zneg:			// -Z
			m_dFacsimileArea = m_dFacsimileAreaZ;
			break;
		case ViewOption_XposYnegZpos:	// +ISOMETRIC
			m_dFacsimileArea = 0;
			break;
		case ViewOption_XnegYposZneg:	// -ISOMETRIC
			m_dFacsimileArea = 0;
			break;
		default:
			return -1;
		}

		if( giViewOption == ViewOption_Xneg ||
			giViewOption == ViewOption_Yneg ||
			giViewOption == ViewOption_Zneg ||
			giViewOption == ViewOption_XposYnegZpos ||
			giViewOption == ViewOption_XnegYposZneg ) {
				return ConvertExitInfo_Success;
		}

		if( giViewOption == ViewOption_Xpos ) {
			m_dFacsimileAreaX = atof(vProjArea[1].c_str());
			m_dFacsimileArea = m_dFacsimileAreaX;
		} else if( giViewOption == ViewOption_Ypos ) {
			m_dFacsimileAreaY = atof(vProjArea[2].c_str());
			m_dFacsimileArea = m_dFacsimileAreaY;
		} else if( giViewOption == ViewOption_Zpos ) {
			m_dFacsimileAreaZ = atof(vProjArea[0].c_str());
			m_dFacsimileArea = m_dFacsimileAreaZ;
		}

		// 
		const char* name = "write.step.unit";
		m_Unit = Interface_Static::CVal( name );

	}
	catch( ... ) {
		return -1;
	}

	return ConvertExitInfo_Success;
}

/////////////////////////////////////////////
// 
/////////////////////////////////////////////
void displayInit(int argc, char* argv[])
{
	try {
cout << "displayInit() glClear" << endl;
		glClear( GL_COLOR_BUFFER_BIT );

cout << "displayInit() connecting X" << endl;
		// create the 3d device
        // Handle(Aspect_DisplayConnection) aDisplayConnection;
        m_DisplayConnection = new Aspect_DisplayConnection( getenv("DISPLAY") );
cout << "displayInit() OpenGL_GraphicDriver" << endl;
        //Handle(Graphic3d_GraphicDriver) Viewer_aGraphicDriver = new OpenGl_GraphicDriver (aDisplayConnection);
        m_theGraphicDriver = new OpenGl_GraphicDriver (m_DisplayConnection);
cout << "displayInit() Downcast" << endl;
        //Handle(OpenGl_GraphicDriver) TheGraphicDriver = Handle(OpenGl_GraphicDriver)::DownCast(Viewer_aGraphicDriver);

    /*
		// create the NT window
		HDC glDc = wglGetCurrentDC();
		HWND hWnd = WindowFromDC( glDc );
    */

cout << "displayInit() new V3d_Viewer" << endl;
		// create the Viewer
		TCollection_ExtendedString a3DName("Visu3D");
		m_myViewer = new V3d_Viewer(
			m_theGraphicDriver,			// the graphic device
			a3DName.ToExtString(),		// view name
			"",							// domain
			1000.0,						// view size
			V3d_XposYnegZpos,			// View projection
			Quantity_NOC_WHITE,			// background color
			V3d_ZBUFFER,
			V3d_GOURAUD,
			V3d_WAIT,
			Standard_True,				// Computed mode
			Standard_True,				// Default Computed mode
			V3d_TEX_NONE
			);

		//m_myViewer->Init();

cout << "displayInit() creating QApplication" << endl;
        m_app = new QApplication(argc, argv);

cout << "displayInit() new QWidget" << endl;
        //QDialog *aWidget = new QDialog;
        //QGLWidget *aWidget = new QGLWidget;
        m_widget = new QWindow;
        m_widget->resize(WINDOW_WIDTH, WINDOW_HEIGHT);
        m_widget->show();
        //Handle(AIS_InteractiveContext) myContext =new AIS_InteractiveContext(m_myViewer);

        Window aHandle = m_widget->winId(); 
        //Handle(Aspect_DisplayConnection) aDispConnection = myContext->CurrentViewer()->Driver()->GetDisplayConnection();
        Handle(Xw_Window) hWnd = new Xw_Window (m_DisplayConnection, aHandle);

		m_myViewer->SetLightOn( new V3d_DirectionalLight(m_myViewer, V3d_XposYposZpos, Quantity_NOC_WHEAT, Standard_True) );
		m_myViewer->SetLightOn( new V3d_AmbientLight(m_myViewer, Quantity_NOC_GRAY) );

		m_myView = m_myViewer->CreateView();

cout << "displayInit() SetWindow" << endl;
		m_myView->SetWindow( hWnd );

		m_myAISContext = new AIS_InteractiveContext( m_myViewer );
		m_myAISContext->SetDisplayMode( AIS_Shaded );

	}
	catch( ... ) {
		exit( ConvertExitInfo_Unexpected );
	}

    //m_myViewer->SetViewOn();
cout << "displayInit() exiting" << endl;

}

/////////////////////////////////////////////
// OUTPUT FOLDER 
/////////////////////////////////////////////
int CheckOutputFile(char *lpszOutDir, char *lpszFileName, int iViewOption)
{
	int	ret;
	char	szChkOutputFile[_MAX_PATH];	// OUTPUT

	try {
		// 
		memset( szChkOutputFile, 0x00, sizeof(szChkOutputFile) );

		strcpy( szChkOutputFile, lpszOutDir );
		strcat( szChkOutputFile,"/" );
		strcat( szChkOutputFile, lpszFileName );
		strcat( szChkOutputFile, "_" );
		strcat( szChkOutputFile, GetViewOption(iViewOption) );
		strcat( szChkOutputFile, FILE_EXT_JPEG );

        ifstream outFile( szChkOutputFile );
		if( outFile.good() ){
			ret = ConvertExitInfo_Success;
		} else {
			ret = ConvertExitInfo_CantCreate;
		}
	}
	catch( ... ) {
		return ConvertExitInfo_Unexpected;
	}

	return ret;
}

/////////////////////////////////////////////
// 
/////////////////////////////////////////////
bool AnalysisDummy(char *lpszTmpPath, char *lpszFileName)
{
	char	csvname[256];			// 
	FILE	*csvfp;
	double	dm0 = 0.0;

	/* csv file open */
	sprintf(csvname, "%s/%s.csv", lpszTmpPath, lpszFileName);
cout << "AnalysisDummy, csvname=" << csvname << endl;
	csvfp = fopen(csvname, "wt" );
	if( csvfp == NULL ){
cout << "AnalysisDummy, returning false" << endl;
		return false;
	}
	/* end */

	// area
	fprintf(csvfp, "%lf\n", dm0 );
	// vol
	fprintf(csvfp, "%lf\n", dm0 );
	// G
	fprintf(csvfp, "%lf,%lf,%lf\n", dm0, dm0, dm0 );
	//val
	fprintf(csvfp, "%lf,%lf,%lf,%lf\n", dm0, dm0, dm0, dm0 );
	//W
	fprintf(csvfp, "%lf,%lf,%lf\n", dm0, dm0, dm0 );
	//Vx
	fprintf(csvfp, "%lf,%lf,%lf\n", dm0, dm0, dm0 );
	//Vy
	fprintf(csvfp, "%lf,%lf,%lf\n", dm0, dm0, dm0 );
	//Vz
	fprintf(csvfp, "%lf,%lf,%lf\n", dm0, dm0, dm0 );
	//val
	fprintf(csvfp, "%lf,%lf,%lf,%lf\n", dm0, dm0, dm0, dm0 );
	//area
	fprintf(csvfp, "%lf,%lf,%lf\n", dm0, dm0, dm0 );

	fclose(csvfp);

	return true;
}

/////////////////////////////////////////////
// 
/////////////////////////////////////////////
unsigned long GetSurfaceNum( const TopoDS_Shape& shape )
{
	unsigned long face_count = 0;
	Prs3d_ShapeTool tool(shape);
	for(tool.InitFace(); tool.MoreFace(); tool.NextFace())
	{
		++face_count;
	}

	return face_count;
}

/////////////////////////////////////////////
// 
/////////////////////////////////////////////
double GetShapeVolume(const TopoDS_Shape& shape)
{
	Standard_Real volume = 0;
	try
	{
#ifdef OCC_VOLUME
		//GProp_GProps prop;
		//BRepGProp::VolumeProperties(shape, prop);
		//mass = fabs(prop.Mass());

		for(TopoDS_Iterator it(shape); it.More(); it.Next())
		{
			TopoDS_Shape one_shape = it.Value();
			if( TopAbs_COMPOUND == one_shape.ShapeType() ) {
				volume += GetShapeVolume(one_shape);
			}
			else if ( TopAbs_SOLID == one_shape.ShapeType() ||
					  TopAbs_COMPSOLID == one_shape.ShapeType())
			{
				GProp_GProps prop;
				BRepGProp::VolumeProperties(one_shape, prop);
				volume += fabs(prop.Mass());
			}
		}

		//TopAbs_ShapeEnum solid_type_kind[] = {
		//	TopAbs_COMPSOLID,
		//	TopAbs_SOLID,
		//	TopAbs_SHELL
		//};
		//const size_t kind_count = sizeof(solid_type_kind)/sizeof(TopAbs_ShapeEnum);

		//for(size_t i = 0; i < kind_count; ++i)
		//{
		//	for (TopExp_Explorer explorer(shape, solid_type_kind[i]); explorer.More(); explorer.Next())
		//	{
		//		GProp_GProps prop;
		//		BRepGProp::VolumeProperties(explorer.Current(), prop);
		//		volume += fabs(prop.Mass());
		//	}
		//}

		//	BRepGProp::LinearProperties(shape, prop);
		//	BRepGProp::SurfaceProperties(shape, prop);

		//Bnd_Box box;
		//BRepBndLib::Add(shape, box);

		//Standard_Real min_coord[3], max_coord[3];
		//box.Get(min_coord[0], min_coord[1], min_coord[2], max_coord[0], max_coord[1], max_coord[2]);
		//Standard_Real box_vol = (max_coord[0] - min_coord[0])*(max_coord[1] - min_coord[1])*(max_coord[2] - min_coord[2]);
#endif
	}
	catch (...)
	{
		return 0;
	}

	return volume;
}

void SignalHandler(int signal)
{
    printf("Signal %d",signal);
    throw "!Access Violation!";
}


/////////////////////////////////////////////
// 
/////////////////////////////////////////////
int main(int argc, char* argv[])
{
#ifdef _DEBUG

	FILE* fp = NULL;
	fopen(&fp, "cadscanner.log", "a");
	for(int i = 0; i < argc; ++i)
	{
		fprintf(fp, "%s ", argv[i]);
	}
	fprintf(fp, "\n");
	fclose(fp);

#endif

    typedef void (*SignalHandlerPointer)(int);

    SignalHandlerPointer previousHandler;
    previousHandler = signal(SIGSEGV , SignalHandler);

	char	szInDir[_MAX_PATH];			// INPUT FOLDER
	char	szExePath[_MAX_PATH];		// exe
	char	szDrive[_MAX_DRIVE];		// 
	char	szDir[_MAX_DIR];			// 
	char	szFileName[_MAX_EXT];		// 
	char	szExt[_MAX_EXT];			// 
	int		iViewOptionArry[8];			// VIEW OPTION
	char	szFileNameRep[_MAX_EXT];	// Report -> Original Data Filename
	char	szExtRep[_MAX_EXT];			// Report
	int		iNoAnalysis = 0;			// No Analysis (0:Analysis 1:No)

	int     exceptionStatus = ConvertExitInfo_Unexpected;

	if( argc < 20 ) {
		return -1;
	}

	vector<string> vAnalysisInfo;	// 
	
	// 
	m_dFacsimileArea = 0;
	memset( szInDir, 0x00, sizeof(szInDir) );
	memset( szExePath, 0x00, sizeof(szExePath) );
	//memset( szDrive, 0x00, sizeof(szDrive) );
	memset( szDir, 0x00, sizeof(szDir) );
	memset( szFileName, 0x00, sizeof(szFileName) );
	memset( szExt, 0x00, sizeof(szExt) );
	memset( gszDir, 0x00, sizeof(gszDir) );
	memset( gszOutDir, 0x00, sizeof(gszOutDir) );
	memset( gsztmpDir, 0x00, sizeof(gsztmpDir) );
	memset( gszFileName, 0x00, sizeof(gszFileName) );
	memset( szFileNameRep, 0x00, sizeof(szFileNameRep) );
	memset( szExtRep, 0x00, sizeof(szExtRep) );

	try {
		// 
		strcpy( gszDir, argv[1] );
		strcpy( gszOutDir, argv[2] );
		if( gszOutDir[strlen(gszOutDir)-1] != '/' ) {
			strcat( gszOutDir,"/Pictures/" );
		} else {
			strcat( gszOutDir,"Pictures/" );
		}

		// VIEW OPTION
		iViewOptionArry[0] = atoi( argv[3] );		// +X
		iViewOptionArry[1] = atoi( argv[4] );		// -X
		iViewOptionArry[2] = atoi( argv[5] );		// +Y
		iViewOptionArry[3] = atoi( argv[6] );		// -Y
		iViewOptionArry[4] = atoi( argv[7] );		// +Z
		iViewOptionArry[5] = atoi( argv[8] );		// -Z
		iViewOptionArry[6] = atoi( argv[9] );		// +ISOMETRIC
		iViewOptionArry[7] = atoi( argv[10] );		// -ISOMETRIC

		// imported after multi support
		strcpy( szExePath, argv[11] );
		// szExePath is taken as the parent folder of Tmp 
		strcpy( gsztmpDir, argv[11] );
		strcat( gsztmpDir, "/StepReader" );
        struct stat s;
        if( stat(gsztmpDir,&s) == 0 ){
            if( s.st_mode & S_IFDIR ){
                // it's a directory
                if( mkdir(gsztmpDir, 0777) != 0 && errno != EEXIST){
                    return -1;
                }
            }
        }
		// imported after multi support end

		giRed = atoi( argv[12] );
		giGreen = atoi( argv[13] );
		giBlue = atoi( argv[14] );

		m_iLicenseCode = atoi( argv[15] );

		giRemoveCurves = atoi( argv[16] );		// Remove Curves
		iNoAnalysis = atoi( argv[19] );		// No Analysis

		// Report
        /*
		_splitpath_s( argv[17], szDrive, sizeof(szDrive), szDir, sizeof(szDir),
			szFileNameRep, sizeof(szFileNameRep), szExtRep, sizeof(szExtRep) );
		strcpy( szInDir, szDrive );
		strcat( szInDir, szDir );
        */
cout << "argv17=" << argv[17]<< endl;
        strcpy( szInDir, argv[17] );
        strcpy( szInDir, dirname(szInDir) );
cout << "dirname=" << szInDir << endl;
cout << "argv17=" << argv[17]<< endl;
        std::string fullname(basename( argv[17] ));
cout << "fullname=" << fullname << endl;
        int lastindex = fullname.find_last_of("."); 
        string rawname = fullname.substr(0, lastindex); 
        strcpy( szFileNameRep, rawname.c_str() );
        
cout << "szFileNameRep=" << szFileNameRep << endl;

		// Report
		strcpy( szExtRep, argv[18] );

		if( (strcasecmp(szExtRep, ".sldprt") == 0) || (strcasecmp(szExtRep, ".sldasm") == 0) ) {
			strcpy( gszFileName, szFileNameRep );
			strcat( gszFileName, "(" );
			strcat( gszFileName, szExtRep );
			strcat( gszFileName, ")" );
		} else {
			//////////////////////////////////////////////////
			// PRO/E .prt.* / .asm.*
			char* extProE = strrchr( szFileNameRep, '.' );
			if( extProE != NULL ) {
				strcpy( gszFileName, szFileNameRep );
				strcat( gszFileName, szExtRep );
			} else {
				// all out filename should include (.<ext>)
				strcpy( gszFileName, szFileNameRep );
				// CAD-13
				strcat( gszFileName, "(" );
				strcat( gszFileName, szExtRep );
				strcat( gszFileName, ")" );
			}
			//////////////////////////////////////////////////
		}

		// 
        /*
		char	tmpImgDrive[_MAX_PATH];
		char	tmpImgDir [_MAX_PATH];
		_splitpath_s( gszDir, tmpImgDrive, sizeof(tmpImgDrive), tmpImgDir, sizeof(tmpImgDir),
			szFileName, sizeof(szFileName), szExt, sizeof(szExt) );
        */
cout << ">>>>> initializing GLUT" << endl;
		glutInit( &argc, argv );
		glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH );
		glutCreateWindow( "OpenGL" );

		glutReshapeWindow( WINDOW_WIDTH, WINDOW_HEIGHT );
		//glutInitWindowPosition( WINDOW_POSITION_X, WINDOW_POSITION_Y );
		
		//glutHideWindow();

cout << ">>>>> importing STEP" << endl;
		//////////////////////////
		// import
		//////////////////////////
		TopoDS_Compound shapes;
		if(!importSTEP(shapes))
		{
			TempReportFile( szInDir, szFileNameRep, szExtRep, ConvertExitInfo_CantRead_ImportStep, NULL, 0, 0, vAnalysisInfo, 0, 0 );
			exit( ConvertExitInfo_CantRead_ImportStep );
		}

		double solid_volume = 0;
		unsigned long ulSurfaceNum = 0;
		if ( iNoAnalysis == 1 ) {
cout << ">>>>> running DUMMY analysis" << endl;

			//////////////////////////
			// 
			//////////////////////////
			ulSurfaceNum = 0;

			//////////////////////////
			// 
			//////////////////////////
			solid_volume = 0.0;

			//////////////////////////
			// export
			//////////////////////////
			bool ret = AnalysisDummy( gsztmpDir, gszFileName );
			if( ret != true ) {
				TempReportFile( szInDir, szFileNameRep, szExtRep, ConvertExitInfo_CantRead_AnalysisDummy, NULL, 0, 0, vAnalysisInfo, 0, 0 );
				exit( ConvertExitInfo_CantRead_AnalysisDummy );
			}
		}
		else {
cout << ">>>>> running analysis" << endl;

			//////////////////////////
			// 
			//////////////////////////
			ulSurfaceNum =  GetSurfaceNum( shapes );
            gSurfaceNum = ulSurfaceNum;

			//////////////////////////
			// 
			//////////////////////////
			solid_volume = GetShapeVolume(shapes);

			//////////////////////////
			// export
			//////////////////////////
			bool ret = exportSTL( shapes );
			if( ret != true ) {
				TempReportFile( szInDir, szFileNameRep, szExtRep, ConvertExitInfo_CantRead_ExportStl, NULL, 0, 0, vAnalysisInfo, 0, 0 );
				exit( ConvertExitInfo_CantRead_ExportStl );
			}

			// 
			// Note: gszFileName does not require .stl extension

            // FIXME
			//DWORD dwExCode = AnalysisExeRun( szExePath, gsztmpDir, gszFileName );
		}

		FILE	*fp;
		int	    error;
		char	szCSVFile[_MAX_PATH];	// 
		char	szLine[1024];
		char	*p;

		// 
		memset( szCSVFile, 0x00, sizeof(szCSVFile) );
		memset( szLine, 0x00, sizeof(szLine) );

		////////////////////////////////////////////
		// 
		////////////////////////////////////////////
		strcpy( szCSVFile, gsztmpDir );
		strcat( szCSVFile,"/" );
		strcat( szCSVFile, gszFileName );
		strcat( szCSVFile, ".csv" );

cout << ">>>>> creating CSV file at :" << szCSVFile << endl;
		// Open
   
        /* 
		if ( (fp = fopen(szCSVFile ,"r")) == 0 ) {
cout << ">>>>> creating CSV file failed, exiting" << endl;
			return ConvertExitInfo_CantCreate;
		}
        */

        char* dummyArray[] = {"0","0","0,0,0","0,0,0,0,","0,0,0","0,0,0","0,0,0","0,0,0","0,0,0,0","0,0,0" };
        for( int i=0; i<10; i++){
			vAnalysisInfo.push_back( dummyArray[i] );
        }
        /*
		while (fgets(szLine, 1024, fp) != NULL) {
			p = strchr( szLine, '\n' );
			if( p != NULL ) {
				*p = '\0';
			}
cout << ">>>>> printing szLine data:" << szLine << endl;
			//vAnalysisInfo.push_back( szLine );
		}

cout << ">>>>> generating CSV" << endl;
		// Close
		fclose( fp );
        */
    
		displayInit(argc, argv);

		m_Unit = "";
		m_dFacsimileArea = 0;
		m_dFacsimileAreaX = 0;
		m_dFacsimileAreaY = 0;
		m_dFacsimileAreaZ = 0;

		//////////////////////////
		// 
		//////////////////////////
		exceptionStatus = ConvertExitInfo_Model_Reg;
		{
			Quantity_Color CSFColor;
			CSFColor = Quantity_Color( giRed / 255., giGreen / 255., giBlue / 255., Quantity_TOC_RGB );
			Handle(AIS_Shape) anAISShape = new AIS_Shape(shapes);
			m_myAISContext->SetMaterial( anAISShape, Graphic3d_NOM_PLASTIC );
			m_myAISContext->SetColor( anAISShape, CSFColor.Name() );
			m_myAISContext->Display( anAISShape, Standard_False);
			//m_myAISContext->RemoveAll();
		}
		exceptionStatus = ConvertExitInfo_Unexpected;

cout << ">>>>> dumping 8 images" << endl;
		for( int i = 0; i < 8; i++ ) {
cout << ">>>>> dumping " << i << "th image" << endl;
			if( iViewOptionArry[i] == 0 ) {
				if( i == 7 ) {

					// 
					m_myAISContext->RemoveAll();

					exit( ConvertExitInfo_Success );
				}
				// 
				continue;
			}

			int	iCode;
			// OUTPUT FOLDER 
cout << "output Folder check: out/filename=" << gszOutDir << "/" << gszFileName << endl;
			iCode = CheckOutputFile( gszOutDir, gszFileName, i );
			//if( iCode == ConvertExitInfo_Success ) {
				giViewOption = i;
                //m_myView->Update();
                //m_myView->Redraw();
                //m_myView->Erase();
				iCode = display();
				if( iCode == ConvertExitInfo_Success ) {
					iCode = projArea(vAnalysisInfo );
				}
			//}

			//////////////////////////
			// TempFile Output
			//////////////////////////
			TempReportFile( szInDir, szFileNameRep, szExtRep, iCode,
				(char*)m_Unit, m_dFacsimileArea, i, vAnalysisInfo, ulSurfaceNum, solid_volume);

			if( i == 7 ) {

cout << "closing X Window" << endl;
                //m_widget->hide();
                //m_widget->close();
cout << "RemoveAll() against AIS context" << endl;
				m_myAISContext->RemoveAll();

cout << "exiting loop" << endl;
				//exit( ConvertExitInfo_Success );
			}
		}
	}
	catch( ... ) {
		TempReportFile( szInDir, szFileNameRep, szExtRep, exceptionStatus, NULL, 0, 0, vAnalysisInfo, 0, 0 );
		exit( exceptionStatus );
	}

    Json::Value root;
    root.clear();
    std::ofstream ofs;

	char szJsonOutDir[_MAX_PATH];
    char baseFilename[_MAX_PATH];
    strcpy(baseFilename, basename( argv[17] ));

    strcpy(szJsonOutDir, gsztmpDir);
    strcat(szJsonOutDir, "/StepReaderOut_");
    strcat(szJsonOutDir, baseFilename);
    strcat(szJsonOutDir, ".json");

    root[baseFilename]["unit"] = m_Unit;
    root[baseFilename]["volume"] = gVolume;
    root[baseFilename]["surfaceArea"] = gSurfaceArea;
    root[baseFilename]["numOfSurface"] = gSurfaceNum;

    ofs.open(szJsonOutDir);
    ofs << root;
    ofs.close();
    
cout << ">>>>> ending program correctly" << endl;
	return 0;
}
