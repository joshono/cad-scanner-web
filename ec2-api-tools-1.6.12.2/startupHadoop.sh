#! /bin/sh
. ./env.sh

#startup slave
ec2-start-instances $EC2SLAVE1_INSTANCEID
sleep 5
ec2-associate-address -i $EC2SLAVE1_INSTANCEID 54.83.1.217

# allow slave to startup before master tries to startup Hadoop
sleep 10

#startup master
ec2-start-instances $EC2MASTER_INSTANCEID
sleep 5
ec2-associate-address -i $EC2MASTER_INSTANCEID 54.83.1.212



