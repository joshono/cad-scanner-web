#! /bin/sh
export CADSCANNER_ROOT=/home/ubuntu/CADScannerWeb
export DOWNLOADER_ROOT=$CADSCANNER_ROOT/django1.4/cad/cadapp/upload
export SLAVEREADERTMP_ROOT=$CADSCANNER_ROOT/readerlink/bin/StepReader
export SLAVEPICTURES_ROOT=$SLAVEREADERTMP_ROOT/Pictures
export MASTERREADERTMP_ROOT=$CADSCANNER_ROOT/cpp/StepReader/Linux/bin/StepReader
export MASTERPICTURES_ROOT=$CADSCANNER_ROOT/django1.4/cad/cadapp/upload/Pictures
