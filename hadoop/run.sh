#! /bin/sh
. ./env.sh

# cleanup 
cd /usr/local/hadoop
sudo -u hadoop bin/hadoop fs -rmr /user/hadoop/caddata-output*
sudo -u hadoop bin/hadoop fs -rmr  /user/hadoop/cache
sudo -u hadoop bin/hadoop fs -rmr  /user/hadoop/caddata

# pack CAD data files and upload to s3
cd $DOWNLOADER_ROOT
jar cvf cachedir.jar *.stp
ls -l *.stp | awk '{print $9}'  > InputFiles.txt
#s3cmd put cachedir.jar s3://cadscannerdata/cache/cachedir.jar
#s3cmd put InputFiles.txt s3://cadscannerdata/caddata/InputFiles.txt

scp -i ~/CADScannerWeb/PKI/eastgpu.pem $DOWNLOADER_ROOT/*.stp ubuntu@ec2-54-83-1-217.compute-1.amazonaws.com:$CADSCANNER_ROOT/readerlink/data/

# run hadoop
cd /usr/local/hadoop
#sudo -u hadoop bin/hadoop dfs -copyFromLocal $DOWNLOADER_ROOT/cachedir.jar /user/hadoop/cache/cachedir.jar
sudo -u hadoop bin/hadoop dfs -copyFromLocal $DOWNLOADER_ROOT/InputFiles.txt /user/hadoop/caddata/InputFiles.txt
#sudo -u hadoop bin/hadoop jar contrib/streaming/hadoop-*streaming*.jar -D mapread.reduce.tasks=0 -archives 'hdfs://ec2-54-83-1-212.compute-1.amazonaws.com:54310/user/hadoop/cache/cachedir.jar' -file /home/ubuntu/CADScannerWeb/hadoop/ReaderMapper.py -mapper /home/ubuntu/CADScannerWeb/hadoop/ReaderMapper.py -input hdfs://ec2-54-83-1-212.compute-1.amazonaws.com:54310/user/hadoop/caddata/InputFiles.txt -output /user/hadoop/caddata-output
sudo -u hadoop bin/hadoop jar contrib/streaming/hadoop-*streaming*.jar -D mapread.reduce.tasks=0 -file /home/ubuntu/CADScannerWeb/hadoop/ReaderMapper.py -mapper /home/ubuntu/CADScannerWeb/hadoop/ReaderMapper.py -input hdfs://ec2-54-83-1-212.compute-1.amazonaws.com:54310/user/hadoop/caddata/InputFiles.txt -output /user/hadoop/caddata-output

# get processed data via scp
scp -i ~/CADScannerWeb/PKI/eastgpu.pem ubuntu@ec2-54-83-1-217.compute-1.amazonaws.com:$SLAVEREADERTMP_ROOT/* $MASTERREADERTMP_ROOT
scp -i ~/CADScannerWeb/PKI/eastgpu.pem ubuntu@ec2-54-83-1-217.compute-1.amazonaws.com:$SLAVEPICTURES_ROOT/* $MASTERPICTURES_ROOT

