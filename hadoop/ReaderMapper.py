#!/usr/bin/env python

from __future__ import print_function
import os,subprocess,sys
import multiprocessing as mp
from Queue import Queue
from threading import Thread
import threading
import time
import socket
import re

# default - ubuntu in Evanston
HOMEDIR = '/home/enspirea'
if re.search('ip-', socket.gethostname()):
    HOMEDIR = '/home/hadoop'

#CACHE_ROOT = 'cachedir.jar/'
MEDIA_ROOT = HOMEDIR + '/CADScannerWeb/readerlink/data/'
READEREXE_ROOT = HOMEDIR + '/CADScannerWeb/readerlink/bin/'
READERTMP_ROOT = READEREXE_ROOT + "StepReader/"

class AsyncReaderProcess(object):
    def __init__ (self, datafile, exename, dataext):
        self.cadfilebody = datafile
        self.exename = exename
        self.cadfileext  = dataext
        self.imgGenParam  = "1 1 1 1 1 1 1 1"
        self.red   = 128
        self.green = 255
        self.blue  = 128
        self.param2 = "0"
        self.param3 = "1"
        self.noAnalysis = 0

    def exeCommand(self, fullCommand, args, newEnv):
        #print fullCommand
        cmd = [fullCommand] + args.split(' ')
        print (cmd)
        with open('/tmp/process.txt', 'w') as output:
            subprocess.Popen(cmd, stdout=output, env=newEnv)

    def runProcess(self):
        print('running process')
        newEnv = os.environ.copy()
        newEnv["LD_LIBRARY_PATH"] = '/home/hadoop/CADScannerWeb/readerlink/bin'
        newEnv["DISPLAY"] = ":0"
        exeFullpath = READEREXE_ROOT + self.exename
        #dataFullpath = CACHE_ROOT + self.cadfilebody + self.cadfileext
        dataFullpath = MEDIA_ROOT + self.cadfilebody + self.cadfileext
        tmpFullpath = MEDIA_ROOT + self.cadfilebody + self.cadfileext
        fullcommand = exeFullpath + ' ' 
        print('running process: fullcommand=', exeFullpath)
        paramList = []
        #paramList.append(exeFullpath)
        paramList.append(tmpFullpath)
        paramList.append(READERTMP_ROOT)
        paramList.append(self.imgGenParam)
        paramList.append(READEREXE_ROOT)
        paramList.append(self.red)
        paramList.append(self.green)
        paramList.append(self.blue)
        paramList.append(self.param2)
        paramList.append(self.param3)
        paramList.append(dataFullpath)
        paramList.append(self.cadfileext)
        paramList.append(self.noAnalysis)

        args = self.generateCommand(paramList)

        self.exeCommand(exeFullpath, args, newEnv)
        
    def generateCommand(self, paramList):
        return ' '.join(str(e) for e in paramList)

class StepReaderProcess(AsyncReaderProcess):
    def __init__(self, datafile, exename='Tutorial', dataext='.stp'):
        AsyncReaderProcess.__init__(self, datafile, exename, dataext)

########################
def run(procInst):
    procInst.runProcess()
    #subprocess.check_call([seawatV4x64, swt_nam],cwd=cwd)
 
def worker(queue):
    """Process files from the queue."""
    for args in iter(queue.get, None):
        try:
            run(args)
        except Exception as e: # catch exceptions to avoid exiting the
                               # thread prematurely
            print('%r failed: %s' % (args, e,), file=sys.stderr)

def processCompleted():
    # check the number of running StepReader
    import os
    pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]

    proc_dump = ''
    for pid in pids:
        try:
            prod_dump = open(os.path.join('/proc', pid, 'cmdline'), 'rb').read()
            import re
            m = re.search('Tutorial', prod_dump)
            if m:
                return False
        except IOError: # proc has already terminated
           continue

    return True

def schedule(q):
    # start threads
    threads = [Thread(target=worker, args=(q,)) for _ in range(mp.cpu_count()-1)]
    for t in threads:
        #t.daemon = True # threads die if the program dies
        t.start()
    for _ in threads: q.put_nowait(None) # signal no more files
    for t in threads: t.join() # wait for completion


########################
def read_input(file=sys.stdin):
    for line in file:
        yield line.split()

def listImages():
    pictureDir = MEDIA_ROOT + 'Pictures'
    for fn in os.listdir(pictureDir):
        print(fn , '\t1')

def listTmp():
    for fn in os.listdir(READERTMP_ROOT):
        print(fn , '\t1')

def main(separator='\t'):
    data = read_input()
    q = Queue()
    for file in data:
        fbody, fext = os.path.splitext(file[0])
        if fext == '.stp':
            procInst = StepReaderProcess(fbody)
            q.put_nowait(procInst)

    schedule(q)

    # monitor process completion
    while True:
        if processCompleted():
           break
        time.sleep(10)

    # list all image/tmp files
    listImages()
    listTmp()
    
sys.stdout= open('/tmp/mapperMainOut.txt', 'w')
main()

#if __name__ == "__main__":
#    main()

#with open('/tmp/end.txt', 'w') as output:
#    sys.stdout=output
#print ('end')
