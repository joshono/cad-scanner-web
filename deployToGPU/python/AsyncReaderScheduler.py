from __future__ import print_function
import os,subprocess,sys
import multiprocessing as mp
from Queue import Queue
from threading import Thread
import threading
import StepReaderProcess
import time


def run(procInst,numOfFiles):
    procInst.runProcess(numOfFiles)
    #subprocess.check_call([seawatV4x64, swt_nam],cwd=cwd)

def worker(queue,numOfFiles):
    """Process files from the queue."""
    for args in iter(queue.get, None):
        try:
            run(args,numOfFiles)
        except Exception as e: # catch exceptions to avoid exiting the
                               # thread prematurely
            print('%r failed: %s' % (args, e,), file=sys.stderr)

def schedule(filesToScan):
    # populate files
    q = Queue()
    for fn in filesToScan:
        fbody, fext = os.path.splitext(fn)
        if fext == '.stp':
            procInst = StepReaderProcess.StepReaderProcess(fbody)
            q.put_nowait(procInst)

    # start threads
    numOfFiles = len(filesToScan)
    threads = [Thread(target=worker, args=(q,numOfFiles)) for _ in range(mp.cpu_count()-1)]
    for t in threads:
        #t.daemon = True # threads die if the program dies
        t.start()
    for _ in threads: q.put_nowait(None) # signal no more files
    for t in threads: t.join() # wait for completion

    # monitor process completion
    while True:
        if processCompleted():
           break
        time.sleep(10)

    print ('#####################finished')

def processCompleted():
    # check the number of running StepReader
    import os
    pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]

    proc_dump = ''
    for pid in pids:
        try:
            prod_dump = open(os.path.join('/proc', pid, 'cmdline'), 'rb').read()
            import re
            m = re.search('Tutorial', prod_dump)
            if m:
                return False
        except IOError: # proc has already terminated
           continue

    return True

def main():
    # populate files
    q = Queue()
    for i in range(1,3):
        procInst = StepReaderProcess.StepReaderProcess(str(i))
        q.put_nowait(procInst)

    # start threads
    threads = [Thread(target=worker, args=(q,)) for _ in range(mp.cpu_count()-1)]
    for t in threads:
        t.daemon = True # threads die if the program dies
        t.start()
    for _ in threads: q.put_nowait(None) # signal no more files
    for t in threads: t.join() # wait for completion

if __name__ == '__main__':
    #mp.freeze_support() # optional if the program is not frozen
    DATAROOTDIR = '/home/enspirea/CADScannerWeb/django1.4/cad/cadapp/upload/'
    listOfFiles = os.listdir(DATAROOTDIR)

    filesToScan = []
    for aFile in listOfFiles:
        fileTrunk, fileExtension = os.path.splitext(aFile)
        if fileExtension not in ['.stp']:
            continue
        filename = fileTrunk + fileExtension
        print('scheduled to scan:')
        print(filename)
        filesToScan.append(filename)

    schedule(filesToScan)
