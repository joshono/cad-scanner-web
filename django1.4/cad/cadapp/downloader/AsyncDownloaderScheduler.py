from __future__ import print_function
import os,subprocess,sys
import multiprocessing as mp
from Queue import Queue
from threading import Thread
import threading
import SiteDownloader

from cad.settings import NUM_CONCURRENT_PROCESSING

def run(procInst):
    #print ('starting download by run() ....')
    procInst.download()
    #subprocess.check_call([seawatV4x64, swt_nam],cwd=cwd)

def worker(queue):
    """Process files from the queue."""
    #print ('worker queue=')
    print (queue)
    for args in iter(queue.get, None):
        try:
            run(args)
        except Exception as e: # catch exceptions to avoid exiting the
                               # thread prematurely
            print('%r failed: %s' % (args, e,), file=sys.stderr)

def schedule(urlsToDownload=None):
    # populate files
    #print ('asynch analyzer, in')
    q = Queue()
    for url in urlsToDownload:
        print ('asynch downloader, url to schedule:%s' % (url))
        procInst = SiteDownloader.SiteDownloader(url)
        q.put_nowait(procInst)
        #q.put(procInst)

    # start threads
    threads = [Thread(target=worker, args=(q,)) for _ in range(NUM_CONCURRENT_PROCESSING)]
    print('threads=')
    print(threads)
    for t in threads:
        #t.daemon = True # threads die if the program dies
        print('####thread starting')
        t.start()
    for _ in threads: q.put_nowait(None) # signal no more files
    #for t in threads: t.join() # wait for completion
    
def main():
    schedule(None)

if __name__ == '__main__':
    urls = []
    f = open('/tmp/SiteList.txt')
    for line in f:
        print('Finding data in:'+line)
        if line[0] == '#':
            continue
        urls.append(line)
    f.close()
    schedule(urls)
