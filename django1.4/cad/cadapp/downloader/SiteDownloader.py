import re
import requests
import urllib
import string
import time
import datetime
from cad.settings import DOWNLOAD_ROOT
#DOWNLOAD_ROOT='/tmp'
from cadapp.models import Archive
import urllib2
from bs4 import BeautifulSoup

DEFAULT_PATTERN = 'href=["]*([(http|https):\/\/]*[%\w\-\/\.]+\.stp)["]*>'

class SiteDownloader:
    def __init__(self, url, pattern=DEFAULT_PATTERN):
        self.url = url
        self.pattern = pattern

    def readFile(self, fileObj):
        while True:
            data = fileObj.readline()
            if not data:
                break
            yield data

    def lookForRef(self, url, pattern):
        resp = requests.get(url)
        soup = BeautifulSoup(urllib2.urlopen(url))
        title = soup.title.string

        # look for a link that follows the given regex pattern
        p = re.compile(pattern)
        #print(resp.text)
        #print('lookForRef, found=')
        #print(p.findall(resp.text))
        return title, p.findall(resp.text)

    def getFilename(self, fileUrlRelPath):
        pattern = '([\w%\-\.]+)$'
        p = re.compile(pattern)

        res = p.search(fileUrlRelPath)
        if res:
            return res.group()

    # must end with '/' if not empty
    def getUrlBase(self, url, fileUrlRelPath):
        # if file url contains http, base is empty
        if fileUrlRelPath[:4] == 'http':
            return ''

        # pick URL for wwwroot
        pattern = '(http[s]*:\/\/[\w\.\-]*[:[\d]*]*)\/.*$'
        if fileUrlRelPath[0] != '/':
            # use current URL root 
            pattern = '(http[s]*:\/\/[\w\.\-:\d\/]*)\/[\w\-\.]*$'

        res = re.match(pattern, url)
        print('urlBase found ='+ res.group(1))
        return res.group(1) + '/'

    def getFile(self, title, url, fileUrlRelPath):

        urlBase  = self.getUrlBase(url, fileUrlRelPath)
        filename = self.getFilename(fileUrlRelPath)

        targetFQFN = urlBase + fileUrlRelPath
        downloadTime = string.split(str(time.time()), '.')[0]
        archiveName = downloadTime + '.' + filename
        localFQFN  = DOWNLOAD_ROOT + 'downloaded/' + archiveName

        print('downloading '+targetFQFN)
        print('local FQFN='+localFQFN)
        urllib.urlcleanup()
        urllib.urlretrieve(targetFQFN, localFQFN)

        # create Archive entry
        print('creating Archive entry:' + archiveName)
        obj = Archive(url=targetFQFN, title=title, archiveName = archiveName)
        obj.save()

    def download(self):
        print( self.url )
        title, files = self.lookForRef(self.url.strip(), self.pattern.strip())

        for aFile in files:
            self.getFile(title, self.url, aFile)
    
        #return files

if __name__ == "__main__":
    print 'run this from web server'
    #downloader = SiteDownloader()
    #downloader.download(None)
