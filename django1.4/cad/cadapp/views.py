""" django view code """
import os
import datetime
import re
import socket 
import json
from pprint import pprint

from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from cadapp.scanner import AsyncReaderScheduler
from cadapp.scanner import AsyncHadoopScheduler
from cadapp.analyzer import AsyncAnalyzerScheduler
from cadapp.tools import ProcessHelper
from cadapp.downloader import AsyncDownloaderScheduler
from cadapp.models import CadDatafile
from cadapp.models import Archive
from cadapp.models import Box
from cadapp.models import Point3
from cadapp.models import Vector3
from cad.settings import SUPPORTED_EXT
from cad.settings import MEDIA_ROOT
from cad.settings import READERTMP_ROOT 
from cad.settings import STLANALYZERTMP_ROOT 
from cad.settings import DOWNLOAD_ROOT
from cad.settings import READER_EXENAME
from cad.settings import STLANALYZER_EXENAME
from cad.settings import DOWNLOADER_NAME

from jinja2 import FileSystemLoader
from jinja2.environment import Environment

from django.views.decorators.csrf \
        import csrf_exempt                                          

# Jinja2 layout
ENV = Environment()
ENV.loader = FileSystemLoader('cadapp/layout')

# Jinja2 for form support
DEFAULT_MIMETYPE =  "text/xml"

def get_files_scanned_before(mongo_instances):
    """ find previously scanned files from database"""
    files_scanned_before = []
    for i in mongo_instances:
        files_scanned_before.append(i.filename)

    return files_scanned_before

def archive_to_schedule():
    """ save archives to data folder """
    # TODO conversion from archive to STE
    # NOW STEP files only

    download_folder = DOWNLOAD_ROOT + '/downloaded'
    reader_outputs = os.listdir(download_folder)
    for filename in reader_outputs:
        matched = re.search(r'[\d][\d]*\.(.*$)', filename)
        try:
            original_filename = matched.group(1)
        except AttributeError:
            continue

        source = download_folder + '/' + filename
        destination = MEDIA_ROOT + '/' + original_filename
        os.rename(source, destination)
        try:
            object_found = CadDatafile.objects.get(filename=original_filename)
            #update
            object_found.createdAt = datetime.datetime.utcnow()
            object_found.scannedAt = datetime.datetime.utcnow()
            object_found.save()
        except ObjectDoesNotExist:
            #create
            obj = CadDatafile(  archiveName=filename, \
                                filename=original_filename, \
                                cadId=original_filename, \
                                cadVersion='1.0', \
                                cadFormat='.stp', \
                                createdAt = datetime.datetime.utcnow())
            obj.save()

def schedule(request):
    """ schedule new CAD file to scan """
    archive_to_schedule()

    # FIXME
    # for now it reads a fixed location - upload folder
    # filename is the identifier that contains both identity and version
    files_new = os.listdir(MEDIA_ROOT)
    #files_scanned_before = CadDatafile.objects.all()
    files_scheduled = []

    for new_file_to_schedule in files_new:
        dummy, file_extension = os.path.splitext(new_file_to_schedule)
        if file_extension not in SUPPORTED_EXT:
            continue

        # insert database
        print 'checking if scan is necessary:', new_file_to_schedule
        try:
            # try get and exception raises when none found
            CadDatafile.objects.get( \
                      filename=new_file_to_schedule)
        except ObjectDoesNotExist:
            print 'scheduling', new_file_to_schedule, 'for the first time'
            obj = CadDatafile(  filename=new_file_to_schedule, \
                                cadId=new_file_to_schedule, \
                                cadVersion='1.0', \
                                cadFormat=file_extension, \
                                createdAt = datetime.datetime.utcnow())
            obj.save()
            files_scheduled.append(new_file_to_schedule)

    template = ENV.get_template('schedule.html')
    rendered = template.render(filelist = files_scheduled)
    
    return HttpResponse(rendered)

def scan(request):
    """ start CAD data scanning """

    # check if the process is still running
    if ProcessHelper.processRunning(READER_EXENAME):
        template = ENV.get_template('scan_busy.html')
        rendered = template.render()
        return HttpResponse(rendered)
        

    # list files scheduled for scan
    files_to_scan = []

    # this won't raise exception even the resultset is null
    file_insts_to_scan = CadDatafile.objects.filter(volume=-1)

    for file_inst in file_insts_to_scan:
        files_to_scan.append(file_inst.filename)

    # pass files to scheduler
    if re.search('ip-', socket.gethostname()):
        # use AWS configuration
        AsyncHadoopScheduler.schedule()
    else:
        AsyncReaderScheduler.schedule(files_to_scan)
   
    template = ENV.get_template('scan.html')
    rendered = template.render(filelist = files_to_scan)
    
    return HttpResponse(rendered)


def analyze(request):
    """ read STL data for analysis """

    # check if the process is still running
    if ProcessHelper.processRunning(STLANALYZER_EXENAME):
        template = ENV.get_template('analyze_busy.html')
        rendered = template.render()
        return HttpResponse(rendered)

    # read stl and import all JSON files onto DB
    reader_outputs = os.listdir(READERTMP_ROOT)
    files_analyzed = []
    for out_file in reader_outputs:
        file_trunk, file_extension = os.path.splitext(out_file)
        if file_extension != '.stl':
            continue

        # filetrunk should be like <fbody>(<ext>)
        print file_trunk
        data_filename_body, data_filename_ext, dummy = \
                re.split(r'[\(\)]', file_trunk)
        original_data_filename = data_filename_body + data_filename_ext
        print 'original data filename=', original_data_filename
 
        # query DB if data was processed before
        try:
            object_found = CadDatafile.objects.get(\
                    filename=original_data_filename)
            if len(object_found.boxes)==0:
                files_analyzed.append(out_file)
        except ObjectDoesNotExist:
            print 'analyzing', original_data_filename, 'for the first time'
            # data was processed before
            files_analyzed.append(out_file)
   
    AsyncAnalyzerScheduler.schedule(files_analyzed)

    template = ENV.get_template('analyze.html')
    rendered = template.render(filelist = files_analyzed)
    
    return HttpResponse(rendered)

def get_column_value(json_data, key):
    """ read JSON value for the given key"""

    if key in json_data:
        return json_data[key]
    return None 

def get_boxes(json_data):
    """ get HTML presentation for all bounding boxes """

    retarray = []
    for box_name in ['alignedBox', 'orientedBox', 'original_box']:
        try:
            box = get_box_instance(json_data[box_name])
            retarray.append(box)
        except KeyError:
            pass

    return retarray
   
def json_to_array(json_filename, dir_to_process):
    """ convert json into array """
    patterns_str = [
            r'^Stl[A-Za-z0-9]+Out_(.+)\(.+\)\.stl\.json$',
            r'^[A-Za-z0-9]+Out_(.+).json$',
    ]
    stl_trunk = dummy = None
    for pattern in patterns_str:
        matched = re.match(pattern, json_filename)
        try:
            stl_trunk, dummy = os.path.splitext(matched.group(1))
            break  # breaks immediately after finding one
        except AttributeError:
            continue 
  
    if not stl_trunk:
        print 'json filename matched but no such file, ignoring'
        return None
        
    # read json data from file and save
    datafile_fullpath = dir_to_process+ '/' + json_filename
    json_data = open(datafile_fullpath)
    data = json.load(json_data)
    pprint(data)
    json_data.close()

    return (stl_trunk, data)

def save_stldata(stldata, stl_filename_trunk):
    """ save stl data to database """
    # process file
    # FIXME, now stp is hardcoded
    cad_fullname = stl_filename_trunk + '.stp'
    try:
        key_for_first = stldata.keys()[0]
        stldata = stldata[key_for_first]
    except KeyError:
        pass

    # find the database entry to update
    print 'cad_fullname for update:', cad_fullname
    object_found = CadDatafile.objects.filter(filename=cad_fullname)
    if object_found:
        print 'updating existing entry for ', cad_fullname
        update_db_record(object_found[0], stldata)
    else:
        print 'creating newEntry for ', cad_fullname
        insert_db_record(cad_fullname, stldata)

def json_to_db(dir_to_process):
    """ convert json into DB insertion format """

    reader_outputs = os.listdir(dir_to_process)
    for out_file in reader_outputs:
        dummy, file_extension = os.path.splitext(out_file)
        if file_extension != '.json':
            continue

        stl_trunk, data = json_to_array(out_file, dir_to_process)

        save_stldata(data, stl_trunk)

def insert_db_record(cad_fullname, cad_data):
    """ create DB record for a given CAD data """

    obj = CadDatafile( 
                filename = cad_fullname, \
                createdAt = get_column_value(cad_data,'createdAt'), \
                cadId = cad_fullname, \
                cadVersion = get_column_value(cad_data,'cadVersion'), \
                cadFormat = get_column_value(cad_data,'cadFormat'), \
                docVersion = get_column_value(cad_data,'docVersion'), \
                numOfSurface = get_column_value(cad_data,'numOfSurface'), \
                surfaceArea = get_column_value(cad_data,'surfaceArea'), \
                unit = get_column_value(cad_data,'unit'), \
                volume = get_column_value(cad_data,'volume'), \
                boxes = get_boxes(cad_data),
    )
    obj.save()

def get_point3_instance(val_dict):
    """ create Point3 instance from given value """
    return Point3(x=val_dict['x'], y=val_dict['y'], z=val_dict['z'])

def get_vector3_instance(val_dict):
    """ create Vector3 instance from given value """
    return Vector3(a1=val_dict['a1'], a2=val_dict['a2'], a3=val_dict['a3'])

def get_box_instance(val_dict):
    """ create Box instance from given value """
    return [Box(boxkind=val_dict['boxkind'], \
               axis0=get_vector3_instance(val_dict['axis0']), \
               axis1=get_vector3_instance(val_dict['axis1']), \
               axis2=get_vector3_instance(val_dict['axis2']), \
               center=get_point3_instance(val_dict['center']), \
               extent0=val_dict['extent0'], \
               extent1=val_dict['extent1'], \
               extent2=val_dict['extent2'], ),]

def box_exists(obj_inst, box_kind):
    """ check if box exists """
    count = 0
    for abox in obj_inst.boxes:
        print 'boxkind=', abox.boxkind, box_kind
        if abox.boxkind == box_kind:
            print 'boxkind found, count=', str(count)
            return count
        count = count + 1
    return None

def update_db_column(obj_inst, key, value):
    """ update DB column for a given key/value """

    if key == 'numOfSurface':
        obj_inst.numOfSurface = value
    elif key == 'surfaceArea':
        obj_inst.surfaceArea = value
    elif key == 'unit':
        obj_inst.unit = value
    elif key == 'volume':
        obj_inst.volume = value
    elif key in ['alignedBox', 'orientedBox', 'original_box']:
        index = box_exists(obj_inst, key)
        if index == None:
            obj_inst.boxes.extend(get_box_instance(value))
        else:
            obj_inst.boxes[index] = get_box_instance(value)[0]
    else:
        print 'key', key, 'not found, update not performed'

def update_db_record(obj_inst, cad_data):
    """ update DB record for a given CAD data """
    for key in cad_data.keys():
        update_db_column(obj_inst, key, cad_data[key])
        obj_inst.save()

def report(request):
    """ generate report for all CAD data in the database """

    return search_submit(report)

def out_boxes(boxes):
    """ generate HTML representation for all bounding boxes """

    # print original/oriented only
    out = "<table>"
    out = out \
          + "<tr><td>box kind</td><td>center</td><td>extent</td></tr>"
    for box in boxes:
        if box.boxkind == 'original_box':
            continue

        out = out + "<tr><td>" + box.boxkind + "</td>"
        out = out + "<td>(" + str(box.center.x) \
                    + ", " + str(box.center.y) \
                    + ", " + str(box.center.z) \
                    + ")</td>"
        out = out + "<td>(" + str(box.extent0) \
                    + ", " + str(box.extent1) \
                    + ", " + str(box.extent2) \
                    + ")</td></tr>"

    out = out + "</table>"

    return out

def original_box(boxes):
    """ generate HTML representation for the original bounding boxe """

    # print original/oriented only
    out = "<table><tr>"
    for box in boxes:
        if box.boxkind != 'original_box':
            continue

        out = out   + "<td>(" \
                    + str(box.extent0) \
                    + ", " + str(box.extent1) \
                    + ", " + str(box.extent2) \
                    + ")</td></tr>"

    out = out + "</table>"

    return out

def download(request):
    """ download CAD data from the Internet """

    # check if the process is still running
    if ProcessHelper.processRunning(DOWNLOADER_NAME):
        template = ENV.get_template('download_busy.html')
        rendered = template.render()
        return HttpResponse(rendered)

    # load all urls from sitelist
    urls = []
    urllist = open(DOWNLOAD_ROOT+'SiteList.txt')
    for line in urllist:
        print('Finding data in:'+line)
        if line[0] == '#':
            continue
        urls.append(line)
    urllist.close()

    AsyncDownloaderScheduler.schedule(urls)

    template = ENV.get_template('download.html')
    rendered = template.render(urllist = urls)
    
    return HttpResponse(rendered)

def home(request):
    """ link to show home """
    template = ENV.get_template('home.html')
    rendered = template.render()
    
    return HttpResponse(rendered)

@csrf_exempt    
def search(request):
    """ search given keywords from database indexes """
    template = ENV.get_template('search.html')
    rendered = template.render()

    return HttpResponse(rendered)

def set_image_files(inst_map, img_filename):
    """ set image files in inst map """

    file_trunk, file_extension = os.path.splitext(img_filename)

    imgfile_x = file_trunk + "(" + file_extension + ")_+X.jpeg"
    imgfile_y = file_trunk + "(" + file_extension + ")_+Y.jpeg"
    imgfile_z = file_trunk + "(" + file_extension + ")_+Z.jpeg"
    imgfile_isometric = file_trunk \
                    + "(" + file_extension + ")_+ISOMETRIC.jpeg"
    path = 'media/Pictures'
    test_image_x = path + '/' + imgfile_x
    test_image_y = path + '/' + imgfile_y
    test_image_z = path + '/' + imgfile_z
    test_image_isometric = path + '/' + imgfile_isometric
    
    inst_map['imgFileX'] = '<img src="' \
                    + test_image_x + '" width="50"></td>'
    inst_map['imgFileY'] = '<img src="' \
                    + test_image_y + '" width="50"></td>'
    inst_map['imgFileZ'] = '<img src="' \
                    + test_image_z + '" width="50"></td>'
    inst_map['imgFileISOMETRIC'] = '<img src="' \
                    + test_image_isometric + '" width="50"></td>'

def generate_inst_map(cad_db_inst):
    """ convert fetched values from DB into array """
    inst_map = {}
    inst_map['filename'] = cad_db_inst.filename
        
    set_image_files(inst_map, cad_db_inst.filename)
        
    inst_map['numSurface']  = str(cad_db_inst.numOfSurface)
    inst_map['surfaceArea'] = str(cad_db_inst.surfaceArea)
    inst_map['unit'] = str(cad_db_inst.unit)
    inst_map['volume'] = str(cad_db_inst.volume)
       
    inst_map['originalbox'] = str(original_box(cad_db_inst.boxes))

    url = '(local)'
    try:
        archive_found = Archive.objects.get(
                            archiveName=cad_db_inst.archiveName)
        url = archive_found.url
        title = archive_found.title
    except ObjectDoesNotExist:
        pass

    inst_map['url'] = url
    inst_map['title'] = title

    return inst_map

@csrf_exempt    
def search_submit(request):
    """ run query for given keywords """
    # check json reports in tmp folder and store them in database
    json_to_db(READERTMP_ROOT)
    json_to_db(STLANALYZERTMP_ROOT)

    # get list of processed data
    try:
        #if request.method == 'POST':
        dataProcessed = CadDatafile.objects.all()
    except:
        # no new objects find
        outmsg = "(no files are available for report)"
        return HttpResponse(outmsg)

    instList = []
    for cadInst in dataProcessed:
        instMap = {}
        instMap['filename'] = cadInst.filename

        # add image (+isometric)
        fileTrunk, fileExtension = os.path.splitext(cadInst.filename)
        imgfileX = fileTrunk + "(" + fileExtension + ")_+X.jpeg"
        imgfileY = fileTrunk + "(" + fileExtension + ")_+Y.jpeg"
        imgfileZ = fileTrunk + "(" + fileExtension + ")_+Z.jpeg"
        imgfileISOMETRIC = fileTrunk + "(" + fileExtension + ")_+ISOMETRIC.jpeg"
        path = 'media/Pictures'
        test_imageX = path + '/' + imgfileX
        test_imageY = path + '/' + imgfileY
        test_imageZ = path + '/' + imgfileZ
        test_imageISOMETRIC = path + '/' + imgfileISOMETRIC
        
        instMap['imgFileX'] = '<img src="' + test_imageX + '" width="50"></td>'
        instMap['imgFileY'] = '<img src="' + test_imageY + '" width="50"></td>'
        instMap['imgFileZ'] = '<img src="' + test_imageZ + '" width="50"></td>'
        instMap['imgFileISOMETRIC'] = '<img src="' + test_imageISOMETRIC + '" width="50"></td>'
        
        instMap['numSurface']  = str(cadInst.numOfSurface)
        instMap['surfaceArea'] = str(cadInst.surfaceArea)
        instMap['unit'] = str(cadInst.unit)
        instMap['volume'] = str(cadInst.volume)
       
        # analyzer
        instMap['originalbox'] = str(originalBox(cadInst.boxes))

        url = '(local)'
        try:
            archiveFound = Archive.objects.get(archiveName=cadInst.archiveName)
            url = archiveFound.url
            title = archiveFound.title
        except:
            pass
        instMap['baseurl'] = url
        instMap['title'] = (title[:30] + '..') if len(title)>30 else title

    # FIXME: This was the original implementation
    #data_processed = CadDatafile.objects.all()

    inst_list = []
    for cad_inst in data_processed:
        inst_map = generate_inst_map(cad_inst)
        inst_list.append(inst_map)

    template = ENV.get_template('search_submit.html')
    rendered = template.render(datalist=inst_list)

    return HttpResponse(rendered)

