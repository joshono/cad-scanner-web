import os
import re

def processRunning(processName):
    pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
    proclist = []
    for pid in pids:
        try:
             proclist.append(open(os.path.join('/proc', pid, 'cmdline'), 'rb').read())
        except IOError:
            continue

    for process in proclist:
        if re.search(processName, process):
            return True;
   
    # process is not running 
    return False;
