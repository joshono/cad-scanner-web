from django.db import models
from djangotoolbox.fields import EmbeddedModelField
from djangotoolbox.fields import ListField

# Create your models here.
class Vector3(models.Model):
    a1          = models.FloatField(default=-1.0)
    a2          = models.FloatField(default=-1.0)
    a3          = models.FloatField(default=-1.0)

class Point3(models.Model):
    x           = models.FloatField(default=-1.0)
    y           = models.FloatField(default=-1.0)
    z           = models.FloatField(default=-1.0)

class Box(models.Model):
    boxkind     = models.CharField(max_length=1024)
    axis0       = EmbeddedModelField('Vector3')
    axis1       = EmbeddedModelField('Vector3')
    axis2       = EmbeddedModelField('Vector3')
    center      = EmbeddedModelField('Point3')
    extent0     = models.FloatField(default=-1.0)
    extent1     = models.FloatField(default=-1.0)
    extent2     = models.FloatField(default=-1.0)

class CadDatafile(models.Model):
    archiveName  = models.CharField(max_length=1024)

    filename    = models.CharField(max_length=1024)
    createdAt   = models.DateTimeField(auto_now_add=True, null=True)
    scannedAt   = models.DateTimeField(auto_now_add=True, null=True)

    cadId       = models.CharField(max_length=1024, null=True)
    cadVersion  = models.CharField(max_length=16, null=True)
    cadFormat   = models.CharField(max_length=16, null=True)
    docVersion  = models.IntegerField(default=-1, null=True)

    # StepReader
    numOfSurface  = models.IntegerField(default=-1, null=True)
    surfaceArea   = models.FloatField(default=-1.0, null=True)
    unit          = models.CharField(max_length=16, null=True)
    volume        = models.FloatField(default=-1.0, null=True)

    # StlAnalyzer
    #alignedBox   = DictField(models.FloatField(), null=True)
    boxes        = ListField(EmbeddedModelField('Box'))

class Archive(models.Model):
    url          = models.CharField(max_length=1024, null=True)
    title        = models.CharField(max_length=1024, null=True)
    archiveName  = models.CharField(max_length=1024, null=True)

