import os,subprocess
from cad.settings import MEDIA_ROOT
from cad.settings import READERTMP_ROOT 
from cad.settings import STLANALYZEREXE_ROOT 
from cad.settings import STLANALYZERTMP_ROOT 

class AsyncAnalyzerProcess(object):
    def __init__ (self, databody, exename, dataext):
        self.cadfilebody = databody
        self.exename = exename
        self.cadfileext  = dataext

    def exeCommand(self, fullCommand, args):
        #print fullCommand
        cmd = [fullCommand] + args.split(' ')
        print cmd
        subprocess.Popen(cmd) #, stderr=subprocess.STDOUT)
        p.communicate()
        #print out

    def runProcess(self):
        exeFullpath = STLANALYZEREXE_ROOT + self.exename
        dataFullpath = READERTMP_ROOT + self.cadfilebody + self.cadfileext
        fullcommand = exeFullpath + ' ' 
        paramList = []
        paramList.append(dataFullpath)
        paramList.append(STLANALYZERTMP_ROOT)

        args = self.generateCommand(paramList)

        self.exeCommand(exeFullpath, args)
        
    def generateCommand(self, paramList):
        return ' '.join(str(e) for e in paramList)

