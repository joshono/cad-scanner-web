from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

from cadapp import views

urlpatterns = patterns('',
    url(r'^home$', views.home, name='home'),
    url(r'^download$', views.download, name='download'),
    url(r'^schedule$', views.schedule, name='schedule'),
    url(r'^scan$', views.scan, name='scan'),
    url(r'^report$', views.report, name='report'),
    url(r'^analyze$', views.analyze, name='analyze'),
    url(r'^search$', views.search, name='search'),
    url(r'^search_submit$', views.search_submit, name='search_submit'),
   
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
