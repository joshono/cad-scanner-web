from __future__ import print_function
import os,subprocess,sys
import multiprocessing as mp
from Queue import Queue
from threading import Thread
import threading
import HadoopScannerProcess

from cad.settings import HADOOP_SCANNER_SCRIPT
#HADOOP_SCANNER_SCRIPT = '/home/ubuntu/CADScannerWeb/hadoop/run.sh'

def run(procInst):
    print('running the process')
    procInst.runProcess()
    #subprocess.check_call([seawatV4x64, swt_nam],cwd=cwd)

def worker(queue):
    """Process files from the queue."""
    print ('worker()')
    for args in iter(queue.get, None):
        try:
            run(args)
        except Exception as e: # catch exceptions to avoid exiting the
                               # thread prematurely
            print('%r failed: %s' % (args, e,), file=sys.stderr)

def schedule():
    print ('schedule()')
    # populate files
    q = Queue()
    procInst = HadoopScannerProcess.HadoopScannerProcess(HADOOP_SCANNER_SCRIPT)
    q.put_nowait(procInst)
    t = Thread(target=worker, args=(q,))
    t.start()
    q.put_nowait(None)

    #t.join() # wait for completion

    
if __name__ == '__main__':
    print ('calling schedule()')
    schedule()
