from __future__ import print_function
import os,subprocess,sys
from Queue import Queue
from threading import Thread
import threading
import StepReaderProcess
from cad.settings import NUM_CONCURRENT_PROCESSING

def run(procInst):
    procInst.runProcess()
    #subprocess.check_call([seawatV4x64, swt_nam],cwd=cwd)

def worker(queue):
    """Process files from the queue."""
    for args in iter(queue.get, None):
        try:
            run(args)
        except Exception as e: # catch exceptions to avoid exiting the
                               # thread prematurely
            print('%r failed: %s' % (args, e,), file=sys.stderr)

def schedule(filesToScan):
    # populate files
    q = Queue()
    for fn in filesToScan:
        fbody, fext = os.path.splitext(fn)
        if fext == '.stp':
            procInst = StepReaderProcess.StepReaderProcess(fbody)
            q.put_nowait(procInst)

    # start threads
    threads = [Thread(target=worker, args=(q,)) for _ in range(NUM_CONCURRENT_PROCESSING)]
    for t in threads:
        #t.daemon = True # threads die if the program dies
        t.start()
    for _ in threads: q.put_nowait(None) # signal no more files
    #for t in threads: t.join() # wait for completion

    
if __name__ == '__main__':
    #mp.freeze_support() # optional if the program is not frozen
    DATAROOTDIR = '/home/enspirea/CADScannerWeb/django1.4/cad/cadapp/uploads/'
    listOfFiles = os.listdir(DATAROOTDIR)

    filesToScan = []
    for aFile in listOfFiles:
        fileTrunk, fileExtension = os.path.splitext(aFile)
        if fileExtension not in ['.stp']:
            continue
        filesToScan.append(DATAROOTDIR + aFile)

    schedule(filesToScan)
