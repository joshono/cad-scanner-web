import subprocess
import os
from cad.settings import HADOOP_SCRIPT_ROOT
#HADOOP_SCRIPT_ROOT = '/home/ubuntu/CADScannerWeb/hadoop'

class HadoopScannerProcess(object):
    def __init__(self, scriptFullpath):
        self.scriptFullpath = scriptFullpath

    def runProcess(self):
        #print fullCommand
        #origDir = os.path.dirname(os.path.realpath(__file__))
        #os.chdir(HADOOP_SCRIPT_ROOT)
        p = subprocess.Popen(self.scriptFullpath, cwd=HADOOP_SCRIPT_ROOT) #, stderr=subprocess.STDOUT)
        p.communicate()
        #print out

if __name__=='__main__':
    scriptPath = '/home/ubuntu/CADScannerWeb/hadoop/run.sh'
    obj = HadoopScannerProcess(scriptPath)
    obj.runProcess()
